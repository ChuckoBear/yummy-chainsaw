set :application, "BoiteABiere"
set :domain,      "vps216646.ovh.net"
set :deploy_to,   "/home/chuck/Workspace/#{domain}"
set :app_path,    "app"
set :user,        "chuck"

set :repository,  "git@github.com:hadeli/yummy-chainsaw.git"
set :scm,         :git
set :branch,      "develop"
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `subversion`, `mercurial`, `perforce`, or `none`

set :model_manager, "doctrine"
# Or: `propel`

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain, :primary => true       # This may be the same as your `Web` server

set  :keep_releases,  3

default_run_options[:pty] = true

# Be more verbose by uncommenting the following line
logger.level = Logger::MAX_LEVEL

# Symfony custom

set :shared_files,      ["app/config/parameters.yml"]
set :shared_children,     [app_path + "/logs", web_path + "/uploads", "vendor"]
set :use_composer, true
set :update_vendors, true

# Multistage extension : Pour gérér aussi bien le serveur de dev que le serveur de prod

set :stages,        %w(production staging)
set :default_stage, "staging"
set :stage_dir,     "app/config"
require 'capistrano/ext/multistage'



