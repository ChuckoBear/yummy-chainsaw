set :application, "BoiteABiere"
set :domain,      "vps216646.ovh.net"
set :deploy_to,   "/home/chuck/Workspace/#{domain}/test"
set :app_path,    "app"
set :user,        "chuck"
set :symfony_env,  "dev"

set :repository,  "git@github.com:hadeli/yummy-chainsaw.git"
set :scm,         :git
set :branch,      "develop"
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `subversion`, `mercurial`, `perforce`, or `none`

set :model_manager, "doctrine"
# Or: `propel`

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain, :primary => true       # This may be the same as your `Web` server

set :use_sudo, true
set  :keep_releases,  3

default_run_options[:pty] = true

# Be more verbose by uncommenting the following line
logger.level = Logger::MAX_LEVEL

# Symfony custom

set :shared_files,      ["app/config/parameters.yml"]
set :shared_children,     [app_path + "/logs", web_path + "/uploads", "vendor"]
set :use_composer, true
set :update_vendors, true

# after "deploy:finalize_update" do
# # run "chown -R www-data:www-data #{latest_release}"
#   run "sudo chmod -R 777 #{latest_release}/#{cache_path}"
#   run "sudo chmod -R 777 #{latest_release}/#{log_path}"
# end

set :clear_controllers, false
set :symfony_env, "dev"

# after "deploy" do
#   transfer :up, "web/app_dev.php", deploy_to+"/current/web/bab_dev.php"
# end
