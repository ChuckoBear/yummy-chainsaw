# BaB
Site Boite à Bière

# Message de Nab

c'est pas tant le cahier des charges que la version dessinée de ce que l'on a discuté.
si on part sur une page dynamique où l'on scrolle (un peu comme le lien de la wine box), il s'agit d'avoir, à l'arrivée sur la page, logo + menus + une première image en lien 
vers la boutique. plutot que du fond texturé carton, je partirais plus sur un fond ocre (proche du carton), ça charge moins l'écran, les seuls visuels réellement chargés sont 
les photos en elles même.
sous le logo, le menu avec connexion / acheter / offrir / nos pts relais / le blog
une fois connecté, il faut remplacer "connexion" par "mon compte", éventuellemnt "mon compte" est déroulant (pour afficher mes factures, mes infos... et me logout)
en dessous du menu, 4 blocs. j'aurais tendance à dire full largeur (en responsive), voir que ça rende bien sur smartphone / tablette (eh ouais mémé)
le premier, la boutique (achat, le second pour offrir, le réseau des pts relais, le dernier le blog (inverser les 2 derniers?). sur le dessin il n'y a que 3 rubriques, mais l'idée est là.
l'idée est que quan tu scrolles vers le bas, le menu reste visible. idem pour le logo, donc vois si on peut le réduire et faire glisser sur le coté du menu en scrollant. 
là encore, je ne suis pas webdesigner, je ne sais pas trop ce qui est possible pour toi wink emoticon
l'autre solution etant d'avoir un menu avec le logo sur un côté, ou garder le logo centré, et faire apparaitre le menu quand on survole le logo BAB, ou, ou, ou... bref, t'as l'idée.
au niveau des photos pour fond de rubriques, il faur qu'on les "pose". celle pour le blog - je pense - est faite (celle avec l'ordi en fond. faudra que je change le contenu de l'écran), il nous en faudra qques unes un peu pluys à thème (genre un apéro avec une carte de france comme nappe par exemple)
en bas, retour à l'ocre, mentions légales, liens...


# Transcription

## Structure du site
### Quelque soit la vue
#### Header
* Logo
    * Lien vers la vue `accueil.php`
    
#### Navbar
* Connexion/"Mon compte"
    * Mes factures
    * Mes informations personnelles
    * Deconnection 
* Acheter 
* Offrir
* Nos points relais
* Le blog


#### Footer
* Retour en haut
* Mentions légales
* Icone reseaux sociaux 
* Lien vers [ici](http://4chan.org/)

### `Accueil`
4 rubriques (les mêmes que la navbar) + reseaux sociaux 
1 photo en fond pour chacun. Si responsive, voir si on ne permutte pas pour des textures générés par le CSS.
Idée : Mettre en surbrillance, dans la nav bar, la rubrique dans laquelle on est.

#### User XP
2 idées :
* Quand on scroll, la navbar reste horizontale et le logo réduit de taille.
* Navbar verticale seulement visible quand on passe la souris en haut à gauche (Logo BaB)


## CSS
* Fond ocre `background: #ffffff;`
* Site responsive (Fuck...)
* Flat design

## Remarques
* Prévoir les trad ?
* Limiter le chargement au max, utiliser le css plutot que des images tant que c'est possible.

## Formulaire d'achat
### Rubriques
### Ergonomie
Paralax