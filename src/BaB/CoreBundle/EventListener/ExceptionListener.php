<?php
/**
 * Created by IntelliJ IDEA.
 * User: chuck
 * Date: 01/12/15
 * Time: 19:11
 */

namespace BaB\CoreBundle\EventListener;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Templating\EngineInterface;

class ExceptionListener
{

    /**
     * The template engine
     *
     * @var EngineInterface
     */
    private $templateEngine;

    /**
     * ExceptionListener constructor.
     * @param EngineInterface $templateEngine
     */
    public function __construct(EngineInterface $templateEngine)
    {
        $this->templateEngine = $templateEngine;
    }


    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        var_dump($event->getException());
        $exception = $event->getException();
        $message = sprintf(
            'My Error says: %s with code: %s',
            $exception->getMessage(),
            $exception->getCode()
        );

        // Customize your response object to display the exception details
        $response = new Response();
        $response->setContent($message);
        $statutTextDetail = "";
        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
            $statutTextDetail = $exception->getMessage();

        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $response = $this->templateEngine->render('@BaBCore/Exception/error.html.twig',array(
            'statut_text_detail' => $exception->getMessage(),
            'statut_code' => $exception->getCode()
        ));
        // Send the modified response object to the event
        $event->setResponse(new Response($response));

    }
}