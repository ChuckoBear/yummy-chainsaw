function loadPointRelais() {
    var pointRelaisProps = pointsRelaisJSON[$('#bab_corebundle_souscription_pointRelais').find(":selected").prop('value')];
    showPointRelais(pointRelaisProps);
}
$(document).ready(function() {
    $('#fullpage').fullpage({
        navigation: true,
        showActiveTooltip: true,
        css3: true,
        scrollingSpeed: 500,
        resize: true,
    });
    fillPrices();
    showModeLivraison();
    $("#bab_corebundle_souscription_modeLivraison").click(function() {
        fillPrices();
        showModeLivraison();
    });
    $("#bab_corebundle_souscription_modeSouscription").click(function() {
        fillPrices()
    });
    $('#bab_corebundle_souscription_pointRelais').click(function(){
        loadPointRelais();
    });
});

var livraisonDict = {};
livraisonDict["Points Relais"] = {divClass:"bab-points-relais", name:"Points Relais"};
livraisonDict["Navette Pickup"]= {divClass:"navette-pickup", name:"Navette Pickup"};
livraisonDict["Colissimo"] = {divClass:"collisimo", name:"Colissimo"};

function isPointRelais() {
    var modeSelected = $('#bab_corebundle_souscription_modeLivraison').find(":selected").text();
    return modeSelected.search(/Points Relais/i) > -1;
}

function showModeLivraison() {
    var modeSelected = $('#bab_corebundle_souscription_modeLivraison').find(":selected").text();
    for (var livraisonKey in livraisonDict) {
        if (livraisonDict.hasOwnProperty(livraisonKey)) {
            var livraisonObject = livraisonDict[livraisonKey];
            //console.log(livraisonKey);
            if(modeSelected.indexOf(livraisonObject.name) > -1) {

                $("."+livraisonObject.divClass).show();
            }
            else {
                $("."+livraisonObject.divClass).hide();
            }
        }
    }
    showAddressForm();
}

function showAddressForm() {
    var modeSelected = $('#bab_corebundle_souscription_modeLivraison').find(":selected").text();

    if (modeSelected.indexOf(livraisonDict["Navette Pickup"].name) > -1 | modeSelected.indexOf(livraisonDict["Colissimo"].name) > -1)  {
        $(".formAddress").show();
        $(".requiredLivraison").prop('required', true);
    }
    else {
        var pointRelaisProps = pointsRelaisJSON[$('#bab_corebundle_souscription_pointRelais').find(":selected").prop('value')];
        showPointRelais(pointRelaisProps);
        $(".formAddress").hide();
        $(".requiredLivraison").prop('required', false);

    }
}

function showPointRelais(pointRelaisJSON) {
    $("#libelle-pt-relais").text(pointRelaisJSON['libelle']);
    $("#adr-pt-relais").text(pointRelaisJSON['adresse']);
    $("#cp-pt-relais").text(pointRelaisJSON['cp']);
    $("#ville-pt-relais").text(pointRelaisJSON['ville']);
    var myLatLng = {lat: parseFloat(pointRelaisJSON['latitude']), lng: parseFloat(pointRelaisJSON['longitude'])};
    if (typeof google === 'object' && typeof google.maps === 'object') {
        var map = new google.maps.Map(document.getElementById('map-pts'), {
            zoom: 10,
            center: myLatLng
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: pointRelaisJSON['libelle']
        });


    }
}


function getPrice($id) {
    var offreSelected = $('#'+$id).find(":selected").text();
    var priceSelected = offreSelected.match(/[0-9]*.[0-9]*€$/i);

    var priceCleared = priceSelected[0].replace(/€/i,'');
    return parseFloat(priceCleared).toFixed(2);
}

function getMonths() {
    var offreSelected = $('#bab_corebundle_souscription_modeSouscription').find(":selected").text();
    var nombreSelected =  offreSelected.match(/[0-9]/i);
    return parseFloat(nombreSelected[0]).toFixed(2);
}

function getTotalPrice() {
    //console.log(getPrice('bab_corebundle_souscription_modeSouscription'));
    //console.log(getMonths() * getPrice('bab_corebundle_souscription_modeLivraison'));
    return parseFloat(getPrice('bab_corebundle_souscription_modeSouscription')) + parseFloat(
            getMonths() * getPrice('bab_corebundle_souscription_modeLivraison'));
}

function fillPrices() {
    $("#totalPrdts").text(getPrice('bab_corebundle_souscription_modeSouscription')+"€");
    var fraisLivr = getMonths() * getPrice('bab_corebundle_souscription_modeLivraison');
    $("#fraisLivr").text(parseFloat(fraisLivr).toFixed(2)+"€");
    $("#totalPrice").text(getTotalPrice()+"€");
}

