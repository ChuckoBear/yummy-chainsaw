<?php

namespace BaB\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{


    public function homeAction()
    {
        return $this->render('@BaBFrontEnd/Default/index.html.twig');
    }

    public function buyAction($mode) {

        return $this->render('@BaBFrontEnd/Default/form_achat.html.twig', array(
            'mode' => $mode
        ));
    }

}
