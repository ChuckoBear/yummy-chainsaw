<?php
/**
 * Created by IntelliJ IDEA.
 * User: chuck
 * Date: 27/11/15
 * Time: 07:49
 */

namespace BaB\CoreBundle\Entity;


use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class BoxRepository extends EntityRepository
{

    /**
     * Permet de récupérer la liste des boxs ouvertes à la vente par rapport à la date
     * @return Box[] La liste des boxs disponibles à cette date
     */
    public function findBoxAvalaibleByDate() {
        $todayDate = new \DateTime();
//        $todayDate->format('Y-m-d');
//        $todayDate = $todayDate->format('Y-m-d');
        $boxQueryBuilder = $this->_em->createQueryBuilder();

        return $boxQueryBuilder
            ->select('bs')
            ->from('BaBCoreBundle:Box', 'bs')
            ->where($boxQueryBuilder->expr()->between('?1', 'bs.dateDebutVenteBox', 'bs.dateFinVenteBox'))
            ->setParameter(1, $todayDate, Type::DATETIME)
            ->getQuery()->getResult();
    }

    /**
     * Permet de récuperer la liste des box disponibles pour un abonnement à partir d'une date de première box
     * @param $dateFirstBox \DateTime La date de la première box choisis
     * @param $nombreMois integer Le nombre de mois demandés
     * @return Box[] Liste des box disponibles pour un abonnement
     */
    public function findBoxSouscripByDate($dateFirstBox, $nombreMois) {
        $boxQueryBuilder = $this->_em->createQueryBuilder();

        return $boxQueryBuilder
            ->select('bs')
            ->from('BaBCoreBundle:Box', 'bs')
            ->where($boxQueryBuilder->expr()->gte('bs.dateSortieBox','?1'))
            ->andWhere($boxQueryBuilder->expr()->eq('bs.gamme', 1))
            ->setParameter(1, $dateFirstBox, TYPE::DATETIME)
            ->setMaxResults($nombreMois)
            ->getQuery()
            ->getResult();
    }


    /**
     * @param $box Box La box dont on souhaite connaitre le nombre de ventes
     * @return int Le nombre de box vendues
     */
    public function countBoxValidee($box) {
        $souscriptionRepository = $this
            ->getEntityManager()
            ->getRepository('BaBCoreBundle:Souscription');

        $souscriptionDetailRepository = $this
            ->getEntityManager()
            ->getRepository('BaBCoreBundle:SouscriptionDetail');

        $souscriptionsPayes = $souscriptionRepository->findSouscriptionPaye();


        $boxCount = 0;
        foreach ($souscriptionsPayes as $souscriptionIdPaye) {
            $souscriptionDetails = $souscriptionDetailRepository->findBySouscription($souscriptionIdPaye);
            foreach ($souscriptionDetails as $souscriptionDetail) {
                if($box->getId() == $souscriptionDetail->getBox()->getId())
                    $boxCount = $boxCount + 1;
            }
        }
        return $boxCount;
    }

    /**
     * @return QueryBuilder Les boxs dont il reste du stock et la plage de date concorde
     */
    public function findBoxAvalaible() {

        $boxQueryBuilder = $this->_em->createQueryBuilder();

        $boxsAvalaibleDateCriteria = $this->findBoxAvalaibleByDate();
        $boxAvailable = [];
        foreach($boxsAvalaibleDateCriteria as $box ) {
//            var_dump($box);
            if($this->countBoxValidee($box) < $box->getNombreBoxProposes()) {
                array_push($boxAvailable, $box->getId());
            }
        }

        return $boxQueryBuilder
            ->select('bs')
            ->from('BaBCoreBundle:Box', 'bs')
            ->where($boxQueryBuilder->expr()->in('bs.id', '?1'))
            ->setParameter(1, $boxAvailable);

    }


}