<?php

namespace BaB\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Box
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="BaB\CoreBundle\Entity\BoxRepository")
 */
class Box
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomBox", type="string", length=255)
     */
    private $nomBox;

    /**
     * @var \DateTime
     * Date de sortie de la box
     * @ORM\Column(name="dateSortieBox", type="date")
     */
    private $dateSortieBox;

    /**
     * @var \DateTime
     * Date de début de mise en vente de la box
     * @ORM\Column(name="date_debut_vente_box", type="date")
     */
    private $dateDebutVenteBox;

    /**
     * @var \DateTime
     * Date de fin de mise en vente de la box
     * @ORM\Column(name="date_fin_vente_box", type="date")
     */
    private $dateFinVenteBox;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionBox", type="text")
     */
    private $descriptionBox;

    /**
     * @var integer
     *
     * @ORM\Column(name="nombreBoxProposes", type="integer")
     */
    private $nombreBoxProposes;

    /**
     * @var float
     *
     * @ORM\Column(name="prix_de_vente_ht", type="decimal", precision=5, scale=2)
     */
    private $prixDeVenteHT;

    /**
     * @var GammeBox
     * @ORM\ManyToOne(targetEntity="BaB\CoreBundle\Entity\GammeBox")
     * @ORM\JoinColumn(nullable=false)
     */
    private $gamme;

    /**
     * @var int
     */
    private $nombreBoxVendues;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomBox
     *
     * @param string $nomBox
     * @return Box
     */
    public function setNomBox($nomBox)
    {
        $this->nomBox = $nomBox;

        return $this;
    }

    /**
     * Get nomBox
     *
     * @return string 
     */
    public function getNomBox()
    {
        return $this->nomBox;
    }

    /**
     * Set dateSortieBox
     *
     * @param \DateTime $dateSortieBox
     * @return Box
     */
    public function setDateSortieBox($dateSortieBox)
    {
        $this->dateSortieBox = $dateSortieBox;

        return $this;
    }

    /**
     * Get dateSortieBox
     *
     * @return \DateTime 
     */
    public function getDateSortieBox()
    {
        return $this->dateSortieBox;
    }

    /**
     * Set descriptionBox
     *
     * @param string $descriptionBox
     * @return Box
     */
    public function setDescriptionBox($descriptionBox)
    {
        $this->descriptionBox = $descriptionBox;

        return $this;
    }

    /**
     * Get descriptionBox
     *
     * @return string 
     */
    public function getDescriptionBox()
    {
        return $this->descriptionBox;
    }

    /**
     * @return int
     */
    public function getNombreBoxProposes()
    {
        return $this->nombreBoxProposes;
    }

    /**
     * @param int $nombreBoxProposes
     */
    public function setNombreBoxProposes($nombreBoxProposes)
    {
        $this->nombreBoxProposes = $nombreBoxProposes;
    }

    /**
     * @return float
     */
    public function getPrixDeVenteHT()
    {
        return $this->prixDeVenteHT;
    }

    /**
     * @param float $prixDeVenteHT
     */
    public function setPrixDeVenteHT($prixDeVenteHT)
    {
        $this->prixDeVenteHT = $prixDeVenteHT;
    }

    /**
     * @return \DateTime
     */
    public function getDateDebutVenteBox()
    {
        return $this->dateDebutVenteBox;
    }

    /**
     * @param \DateTime $dateDebutVenteBox
     */
    public function setDateDebutVenteBox($dateDebutVenteBox)
    {
        $this->dateDebutVenteBox = $dateDebutVenteBox;
    }

    /**
     * @return \DateTime
     */
    public function getDateFinVenteBox()
    {
        return $this->dateFinVenteBox;
    }

    /**
     * @param \DateTime $dateFinVenteBox
     */
    public function setDateFinVenteBox($dateFinVenteBox)
    {
        $this->dateFinVenteBox = $dateFinVenteBox;
    }

    function __toString()
    {
        return $this->getNomBox();
    }

    /**
     * @return mixed
     */
    public function getGamme()
    {
        return $this->gamme;
    }

    /**
     * @param mixed $gamme
     */
    public function setGamme($gamme)
    {
        $this->gamme = $gamme;
    }

    /**
     * @return int
     */
    public function getNombreBoxVendues()
    {
        return $this->nombreBoxVendues;
    }

    /**
     * @param int $nombreBoxVendues
     */
    public function setNombreBoxVendues($nombreBoxVendues)
    {
        $this->nombreBoxVendues = $nombreBoxVendues;
    }





}
