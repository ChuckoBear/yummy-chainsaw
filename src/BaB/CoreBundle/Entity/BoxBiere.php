<?php

namespace BaB\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BoxBiere
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class BoxBiere
{
    /**
     * @var Box
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Box")
     */
    private $box;

    /**
     * @var Biere
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Biere")
     */
    private $biere;

    /**
     * @var integer
     *
     * @ORM\Column(name="nombreBieres", type="integer")
     */
    private $nombreBieres;




    /**
     * Set nombreBieres
     *
     * @param integer $nombreBieres
     * @return BoxBiere
     */
    public function setNombreBieres($nombreBieres)
    {
        $this->nombreBieres = $nombreBieres;

        return $this;
    }

    /**
     * Get nombreBieres
     *
     * @return integer 
     */
    public function getNombreBieres()
    {
        return $this->nombreBieres;
    }

    /**
     * Set box
     *
     * @param Box $box
     * @return BoxBiere
     */
    public function setBox(Box $box)
    {
        $this->box = $box;

        return $this;
    }

    /**
     * Get box
     *
     * @return Box
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * Set biere
     *
     * @param Biere $biere
     * @return BoxBiere
     */
    public function setBiere(Biere $biere)
    {
        $this->biere = $biere;

        return $this;
    }

    /**
     * Get biere
     *
     * @return Biere
     */
    public function getBiere()
    {
        return $this->biere;
    }
}
