<?php
/**
 * Created by IntelliJ IDEA.
 * User: Gecko
 * Date: 05/08/2015
 * Time: 15:31
 */

namespace BaB\CoreBundle\Entity;


use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;

class MouvementStockRepository extends EntityRepository
{

    /**
     * Récupère la listes des bières pour lesquels on a des mouvements de stock
     * @param \DateTime $dateBorneSuperieure La date de début pour la recherche. Par défault, c'est la date du jour.
     * @param \DateTime $dateBorneInferieure La date de fin pour la recherche. Par défault, c'est le 01/01/1970 00:00:00
     * @return array La liste des bières qui ont eu des mouvements de stock pendant la période défini en paramètre.
     */
    public function findByEtatStockGroupedById(\DateTime $dateBorneSuperieure = null,
                                               \DateTime $dateBorneInferieure = null) {
        if (null === $dateBorneSuperieure) {
            $dateBorneSuperieure = new \DateTime();
        }

        if (null === $dateBorneInferieure) {
            $dateBorneInferieure = new \DateTime('1970-01-01');
        }
        $mouvementsQueryBuilder = $this->_em
            ->createQueryBuilder();

        return $mouvementsQueryBuilder
            ->select('biere.id')
            ->distinct()
            ->from('BaBCoreBundle:MouvementStock', 'ms')
            ->leftJoin('ms.biere', 'biere')
            ->where($mouvementsQueryBuilder->expr()->between('ms.dateMouvementStock', '?1', '?2'))
            ->setParameter(1, $dateBorneInferieure, Type::DATETIME)
            ->setParameter(2, $dateBorneSuperieure, Type::DATETIME)
            ->getQuery()->getScalarResult();
    }
}