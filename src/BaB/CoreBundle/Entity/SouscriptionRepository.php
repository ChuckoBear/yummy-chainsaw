<?php
/**
 * Created by IntelliJ IDEA.
 * User: chuck
 * Date: 27/11/15
 * Time: 07:46
 */

namespace BaB\CoreBundle\Entity;


use Doctrine\ORM\EntityRepository;

class SouscriptionRepository extends EntityRepository
{
    /**
     * @return Souscription[] Liste des identifiants des souscriptions payes
     */
    public function findSouscriptionPaye() {

        $etatCommandeObj = $this->getEntityManager()->getRepository('BaBCoreBundle:EtatCommande')->findOneBy(array(
            'libelle' => "Commande en préparation"
        ));

        return $this->findBy(array(
            'etatCommande' => $etatCommandeObj
        ));

    }
}