<?php

namespace BaB\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SouscriptionDetail
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="BaB\CoreBundle\Entity\SouscriptionDetailRepository")
 */
class SouscriptionDetail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Souscription
     *
     * @ORM\ManyToOne(targetEntity="BaB\CoreBundle\Entity\Souscription")
     * @ORM\JoinColumn(nullable=false)
     */
    private $souscription;

    /**
     * @var Box
     * @ORM\ManyToOne(targetEntity="BaB\CoreBundle\Entity\Box")
     * @ORM\JoinColumn(nullable=false)
     */
    private $box;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Souscription
     */
    public function getSouscription()
    {
        return $this->souscription;
    }

    /**
     * @param Souscription $souscription
     */
    public function setSouscription($souscription)
    {
        $this->souscription = $souscription;
    }

    /**
     * @return Box
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * @param Box $box
     */
    public function setBox($box)
    {
        $this->box = $box;
    }


}
