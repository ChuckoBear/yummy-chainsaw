<?php

namespace BaB\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BaB\CoreBundle\Entity\MouvementStockRepository;

/**
 * MouvementStock
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="BaB\CoreBundle\Entity\MouvementStockRepository")
 */
class MouvementStock
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="natureMouvementStock", type="string", length=255)
     */
    private $natureMouvementStock;

    /**
     * @var Date
     * @ORM\Column(name="dateMouvementStock", type="date")
     */
    private $dateMouvementStock;

    /**
     * @var string
     *
     * @ORM\Column(name="typeOperationStock", type="string")
     */
    private $typeOperationStock;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantiteMouvementStock", type="integer")
     */
    private $quantiteMouvementStock;

    /**
     * @var string
     *
     * @ORM\Column(name="valeurUnitaire", type="decimal", precision=10, scale=5)
     */
    private $valeurUnitaire;

    /**
     * @var Biere
     *
     * @ORM\ManyToOne(targetEntity="BaB\CoreBundle\Entity\Biere")
     * @ORM\JoinColumn(nullable=false)
     */
    private $biere;


    private $soldeBieres;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set natureMouvementStock
     *
     * @param string $natureMouvementStock
     * @return MouvementStock
     */
    public function setNatureMouvementStock($natureMouvementStock)
    {
        $this->natureMouvementStock = $natureMouvementStock;

        return $this;
    }

    /**
     * Get natureMouvementStock
     *
     * @return string 
     */
    public function getNatureMouvementStock()
    {
        return $this->natureMouvementStock;
    }

    /**
     * @return Date
     */
    public function getDateMouvementStock()
    {
        return $this->dateMouvementStock;
    }

    /**
     * @param Date $dateMouvementStock
     */
    public function setDateMouvementStock($dateMouvementStock)
    {
        $this->dateMouvementStock = $dateMouvementStock;
    }

    /**
     * Set quantiteMouvementStock
     *
     * @param integer $quantiteMouvementStock
     * @return MouvementStock
     */
    public function setQuantiteMouvementStock($quantiteMouvementStock)
    {
        $this->quantiteMouvementStock = $quantiteMouvementStock;

        return $this;
    }

    /**
     * Get quantiteMouvementStock
     *
     * @return integer 
     */
    public function getQuantiteMouvementStock()
    {
        return $this->quantiteMouvementStock;
    }

    /**
     * Set valeurUnitaire
     *
     * @param string $valeurUnitaire
     * @return MouvementStock
     */
    public function setValeurUnitaire($valeurUnitaire)
    {
        $this->valeurUnitaire = $valeurUnitaire;

        return $this;
    }

    /**
     * Get valeurUnitaire
     *
     * @return string 
     */
    public function getValeurUnitaire()
    {
        return $this->valeurUnitaire;
    }

    /**
     * @return mixed
     */
    public function getTypeOperationStock()
    {
        return $this->typeOperationStock;
    }

    /**
     * @param mixed $typeOperationStock
     */
    public function setTypeOperationStock($typeOperationStock)
    {
        $this->typeOperationStock = $typeOperationStock;
    }

    /**
     * @return mixed
     */
    public function getBiere()
    {
        return $this->biere;
    }

    /**
     * @param mixed $biere
     */
    public function setBiere($biere)
    {
        $this->biere = $biere;
    }

    /**
     * @return mixed
     */
    public function getSoldeBieres()
    {
        return $this->soldeBieres;
    }

    /**
     * @param mixed $soldeBieres
     */
    public function setSoldeBieres($soldeBieres)
    {
        $this->soldeBieres = $soldeBieres;
    }




}
