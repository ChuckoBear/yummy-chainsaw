<?php

namespace BaB\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContactBrasserie
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ContactBrasserie
{


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomContact", type="string", length=255)
     */
    private $nomContact;

    /**
     * @var string
     *
     * @ORM\Column(name="prenomContact", type="string", length=255)
     */
    private $prenomContact;

    /**
     * @var string
     *
     * @ORM\Column(name="emailContact", type="string", length=255)
     */
    private $emailContact;

    /**
     * @var string
     *
     * @ORM\Column(name="telephoneContact", type="string", length=20)
     */
    private $telephoneContact;

    /**
     * @var string
     *
     * @ORM\Column(name="posteContact", type="string", length=255)
     */
    private $posteContact;

    /**
     * @var Brasserie
     * @ORM\ManyToOne(targetEntity="BaB\CoreBundle\Entity\Brasserie")
     * @ORM\JoinColumn(nullable=false)
     */
    private $brasserie;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomContact
     *
     * @param string $nomContact
     * @return ContactBrasserie
     */
    public function setNomContact($nomContact)
    {
        $this->nomContact = $nomContact;

        return $this;
    }

    /**
     * Get nomContact
     *
     * @return string 
     */
    public function getNomContact()
    {
        return $this->nomContact;
    }

    /**
     * Set prenomContact
     *
     * @param string $prenomContact
     * @return ContactBrasserie
     */
    public function setPrenomContact($prenomContact)
    {
        $this->prenomContact = $prenomContact;

        return $this;
    }

    /**
     * Get prenomContact
     *
     * @return string 
     */
    public function getPrenomContact()
    {
        return $this->prenomContact;
    }

    /**
     * Set emailContact
     *
     * @param string $emailContact
     * @return ContactBrasserie
     */
    public function setEmailContact($emailContact)
    {
        $this->emailContact = $emailContact;

        return $this;
    }

    /**
     * Get emailContact
     *
     * @return string 
     */
    public function getEmailContact()
    {
        return $this->emailContact;
    }

    /**
     * Set telephoneContact
     *
     * @param string $telephoneContact
     * @return ContactBrasserie
     */
    public function setTelephoneContact($telephoneContact)
    {
        $this->telephoneContact = $telephoneContact;

        return $this;
    }

    /**
     * Get telephoneContact
     *
     * @return string 
     */
    public function getTelephoneContact()
    {
        return $this->telephoneContact;
    }

    /**
     * Set posteContact
     *
     * @param string $posteContact
     * @return ContactBrasserie
     */
    public function setPosteContact($posteContact)
    {
        $this->posteContact = $posteContact;

        return $this;
    }

    /**
     * Get posteContact
     *
     * @return string 
     */
    public function getPosteContact()
    {
        return $this->posteContact;
    }

    /**
     * @return Brasserie
     */
    public function getBrasserie()
    {
        return $this->brasserie;
    }

    /**
     * @param Brasserie $brasserie
     */
    public function setBrasserie($brasserie)
    {
        $this->brasserie = $brasserie;
    }
}
