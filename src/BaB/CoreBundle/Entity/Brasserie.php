<?php

namespace BaB\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Brasserie
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Brasserie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="nomBrasserie", type="string", length=255)
     *
     */
    private $nomBrasserie;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="text", nullable=true)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="cpBrasserie", type="string", length=10)
     */
    private $cpBrasserie;

    /**
     * @var string
     *
     * @ORM\Column(name="villeBrasserie", type="string", length=255)
     */
    private $villeBrasserie;


    /**
     * @var string
     *
     * @ORM\Column(name="paysBrasserie", type="string", length=255)
     */
    private $paysBrasserie;


    /**
     * @var string
     * @ORM\Column(name="telephoneBrasserie", type="string", length=15, nullable=true)
     */
    private $telephoneBrasserie;

    /**
     * @var string
     * @Assert\Email
     * @ORM\Column(name="emailBrasserie", type="string", length=255, nullable=true)
     */
    private $emailBrasserie;

    /**
     * @var string
     * @Assert\Url
     * @ORM\Column(name="siteWebBrasserie", type="string", length=255, nullable=true)
     */
    private $siteWebBrasserie;

    /**
     * Date de création de la brasserie
     * @var mixed
     * @Assert\Date
     * @ORM\Column(name="dateCreationBrasserie", type="date", nullable=true)
     */
    private $dateCreationBrasserie;

    /**
     * @var mixed
     *
     * @ORM\Column(name="nombreEmployeesBrasserie", type="integer", nullable=true)
     */
    private $nombreEmployeesBrasserie;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomBrasserie
     *
     * @param string $nomBrasserie
     * @return Brasserie
     */
    public function setNomBrasserie($nomBrasserie)
    {
        $this->nomBrasserie = $nomBrasserie;

        return $this;
    }

    /**
     * Get nomBrasserie
     *
     * @return string 
     */
    public function getNomBrasserie()
    {
        return $this->nomBrasserie;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Brasserie
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set cpBrasserie
     *
     * @param string $cpBrasserie
     * @return Brasserie
     */
    public function setCpBrasserie($cpBrasserie)
    {
        $this->cpBrasserie = $cpBrasserie;

        return $this;
    }

    /**
     * Get cpBrasserie
     *
     * @return string 
     */
    public function getCpBrasserie()
    {
        return $this->cpBrasserie;
    }

    /**
     * Set villeBrasserie
     *
     * @param string $villeBrasserie
     * @return Brasserie
     */
    public function setVilleBrasserie($villeBrasserie)
    {
        $this->villeBrasserie = $villeBrasserie;

        return $this;
    }

    /**
     * Get villeBrasserie
     *
     * @return string 
     */
    public function getVilleBrasserie()
    {
        return $this->villeBrasserie;
    }

    /**
     * @return string
     */
    public function getPaysBrasserie()
    {
        return $this->paysBrasserie;
    }

    /**
     * Modifie le nom du pays par la valeur passé en paramètre
     *
     * @param string $paysBrasserie
     * @return Brasserie
     */
    public function setPaysBrasserie($paysBrasserie)
    {
        $this->paysBrasserie = $paysBrasserie;

        return $this;
    }

    /**
     * Set telephoneBrasserie
     *
     * @param string $telephoneBrasserie
     * @return Brasserie
     */
    public function setTelephoneBrasserie($telephoneBrasserie)
    {
        $this->telephoneBrasserie = $telephoneBrasserie;

        return $this;
    }

    /**
     * Get telephoneBrasserie
     *
     * @return string 
     */
    public function getTelephoneBrasserie()
    {
        return $this->telephoneBrasserie;
    }

    /**
     * Set emailBrasserie
     *
     * @param string $emailBrasserie
     * @return Brasserie
     */
    public function setEmailBrasserie($emailBrasserie)
    {
        $this->emailBrasserie = $emailBrasserie;

        return $this;
    }

    /**
     * Get emailBrasserie
     *
     * @return string 
     */
    public function getEmailBrasserie()
    {
        return $this->emailBrasserie;
    }

    /**
     * Set siteWebBrasserie
     *
     * @param string $siteWebBrasserie
     * @return Brasserie
     */
    public function setSiteWebBrasserie($siteWebBrasserie)
    {
        $this->siteWebBrasserie = $siteWebBrasserie;

        return $this;
    }

    /**
     * Get siteWebBrasserie
     *
     * @return string 
     */
    public function getSiteWebBrasserie()
    {
        return $this->siteWebBrasserie;
    }

    /**
     * @return mixed
     */
    public function getDateCreationBrasserie()
    {
        return $this->dateCreationBrasserie;
    }

    /**
     * @param mixed $dateCreationBrasserie
     */
    public function setDateCreationBrasserie($dateCreationBrasserie)
    {
        $this->dateCreationBrasserie = $dateCreationBrasserie;
    }

    /**
     * @return mixed
     */
    public function getNombreEmployeesBrasserie()
    {
        return $this->nombreEmployeesBrasserie;
    }

    /**
     * @param mixed $nombreEmployeesBrasserie
     */
    public function setNombreEmployeesBrasserie($nombreEmployeesBrasserie)
    {
        $this->nombreEmployeesBrasserie = $nombreEmployeesBrasserie;
    }

    function __toString()
    {
        return $this->nomBrasserie;
    }
}
