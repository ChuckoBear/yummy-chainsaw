<?php

namespace BaB\CoreBundle\Entity;

use BaB\UserBundle\Entity\Address;
use BaB\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Souscription
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="BaB\CoreBundle\Entity\SouscriptionRepository")
 */
class Souscription
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="BaB\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var ModeSouscription
     *
     * @ORM\ManyToOne(targetEntity="BaB\CoreBundle\Entity\ModeSouscription")
     * @ORM\JoinColumn(nullable=false)
     */
    private $modeSouscription;

    /**
     * @var Box
     *
     * @ORM\ManyToOne(targetEntity="BaB\CoreBundle\Entity\Box")
     * @ORM\JoinColumn(nullable=false)
     */
    private $box;

    /**
     * @var ModeLivraison
     *
     * @ORM\ManyToOne(targetEntity="BaB\CoreBundle\Entity\ModeLivraison")
     * @ORM\JoinColumn(nullable=false)
     */
    private $modeLivraison;

    /**
     * @var PointRelais
     *
     * @ORM\ManyToOne(targetEntity="BaB\CoreBundle\Entity\PointRelais")
     * @ORM\JoinColumn(nullable=true)
     */
    private $pointRelais;

    /**
     * @var EtatCommande
     *
     * @ORM\ManyToOne(targetEntity="BaB\CoreBundle\Entity\EtatCommande")
     * @ORM\JoinColumn(nullable=true)
     */
    private $etatCommande;

    /**
     * @var String
     *
     * @ORM\Column(type="string", length=255, name="etat_paypal", nullable=true)
     */
    private $etatPaypal;

    /**
     * @var String
     *
     * @ORM\Column(name="transaction_id", type="string", length=255, nullable=true)
     */
    private $transactionId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="debut_souscription_date")
     */
    private $debutSouscriptionDate;

    /**
     * @var float
     *
     * @ORM\Column(name="prix_total", type="decimal", precision=10, scale=2)
     */
    private $prixTotal;

    /**
     * @var Address
     *
     * @ORM\ManyToOne(targetEntity="BaB\UserBundle\Entity\Address")
     * @ORM\JoinColumn(nullable=true)
     *
     */
    private $addressLivraison;

    /**
     *
     * @var Address
     * @ORM\ManyToOne(targetEntity="BaB\UserBundle\Entity\Address")
     * @ORM\JoinColumn(nullable=true)
     */
    private $addressFacturation;


    /**
     * @var string
     * @ORM\Column(type="text", length=255, name="commentaire", nullable=true)
     */
    private $commentaire;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return ModeSouscription
     */
    public function getModeSouscription()
    {
        return $this->modeSouscription;
    }

    /**
     * @param ModeSouscription $modeSouscription
     */
    public function setModeSouscription($modeSouscription)
    {
        $this->modeSouscription = $modeSouscription;
    }

    /**
     * @return Box
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * @param Box $box
     */
    public function setBox($box)
    {
        $this->box = $box;
    }

    /**
     * @return ModeLivraison
     */
    public function getModeLivraison()
    {
        return $this->modeLivraison;
    }

    /**
     * @param ModeLivraison $modeLivraison
     */
    public function setModeLivraison($modeLivraison)
    {
        $this->modeLivraison = $modeLivraison;
    }

    /**
     * @return PointRelais
     */
    public function getPointRelais()
    {
        return $this->pointRelais;
    }

    /**
     * @param PointRelais $pointRelais
     */
    public function setPointRelais($pointRelais)
    {
        $this->pointRelais = $pointRelais;
    }

    /**
     * @return EtatCommande
     */
    public function getEtatCommande()
    {
        return $this->etatCommande;
    }

    /**
     * @param EtatCommande $etatCommande
     */
    public function setEtatCommande($etatCommande)
    {
        $this->etatCommande = $etatCommande;
    }

    /**
     * @return float
     */
    public function getPrixTotal()
    {
        return $this->prixTotal;
    }

    /**
     * @param float $prixTotal
     */
    public function setPrixTotal($prixTotal)
    {
        $this->prixTotal = $prixTotal;
    }

    /**
     * @return mixed
     */
    public function getEtatPaypal()
    {
        return $this->etatPaypal;
    }

    /**
     * @param mixed $etatPaypal
     */
    public function setEtatPaypal($etatPaypal)
    {
        $this->etatPaypal = $etatPaypal;
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param mixed $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return mixed
     */
    public function getDebutSouscriptionDate()
    {
        return $this->debutSouscriptionDate;
    }

    /**
     * @param mixed $debutSouscriptionDate
     */
    public function setDebutSouscriptionDate($debutSouscriptionDate)
    {
        $this->debutSouscriptionDate = $debutSouscriptionDate;
    }

    /**
     * @return Address
     */
    public function getAddressLivraison()
    {
        return $this->addressLivraison;
    }

    /**
     * @param mixed $addressLivraison
     */
    public function setAddressLivraison($addressLivraison)
    {
        $this->addressLivraison = $addressLivraison;
    }

    /**
     * @return Address
     */
    public function getAddressFacturation()
    {
        return $this->addressFacturation;
    }

    /**
     * @param mixed $addressFacturation
     */
    public function setAddressFacturation($addressFacturation)
    {
        $this->addressFacturation = $addressFacturation;
    }

    /**
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param string $commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }


}
