<?php
/**
 * Created by IntelliJ IDEA.
 * User: chuck
 * Date: 24/11/15
 * Time: 09:01
 */

namespace BaB\CoreBundle\Entity;


class Paypal
{
    private $user = "";
    private $pwd = "";
    private $signature = "";
    private $userProd = "contact_api1.boiteabiere.fr";
    private $pwdProd = "GTPE4ATQ7PEMJSEK";
    private $signatureProd = "A9loOo.8KClTBQ7LMNodgy1v5tg4AS.daBimXJvC2f0XNVDUxU7EmP5X";
    private $userDev = "contact-facilitator_api1.boiteabiere.fr";
    private $pwdDev = "JHZPZDSLYZVWZTSE";
    private $signatureDev = "AFcWxV21C7fd0v3bYYYRCpSSRl31A6xJTnegjd7EOYREdBaatXH5qE3f";
    private $endpoint = "https://api-3T.sandbox.paypal.com/nvp";
    private $version = "109.0";
    public $errors = array();
    private $brand = "Boite à Bière";

    /**
     * Paypal constructor.
     * @param bool|string $user
     * @param bool|string $pwd
     * @param bool|string $signature
     * @param bool $prod
     */
    public function __construct($user = false, $pwd = false, $signature = false, $prod = false)
    {
        if ($user) {
            $this->user = $user;
        }
        if ($pwd) {
            $this->pwd = $pwd;
        }
        if ($signature) {
            $this->signature = $signature;
        }

        if($prod) {
            $this->user = $this->userProd;
            $this->pwd = $this->pwdProd;
            $this->signature = $this->signatureProd;

            $this->endpoint = str_replace('sandbox.','',$this->endpoint);
//            var_dump($this->endpoint);
        }
        else {
            $this->user = $this->userDev;
            $this->pwd = $this->pwdDev;
            $this->signature = $this->signatureDev;
        }


    }

    public function request($method, $paramsQuery) {
        $paramsQuery = array_merge($paramsQuery, array(
            "METHOD" => $method,
            "VERSION" => $this->getVersion(),
            "USER" => $this->getUser(),
            "SIGNATURE" => $this->getSignature(),
            "PWD" => $this->getPwd(),
            "BRANDNAME" => $this->getBrand()
        ));
        $params = http_build_query($paramsQuery);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->getEndpoint(),
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_VERBOSE => 1
        ));

        $response = curl_exec($curl);
        $responseArray = array();
        parse_str($response, $responseArray);
//        var_dump($response);
        if (curl_errno($curl)) {
            //TODO : Gestion d'erreur CURL
            $this->errors = curl_error($curl);
            curl_close($curl);
            return false;
        }
        //En cas de succès de la requete
        if($responseArray['ACK'] == 'Success') {
            curl_close($curl);

            return $responseArray;
        }
        else {
            $this->errors = $responseArray;
            curl_close($curl);
            return false;
        }
    }


    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPwd()
    {
        return $this->pwd;
    }

    /**
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @return mixed|string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }








}