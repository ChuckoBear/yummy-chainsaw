<?php

namespace BaB\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PointRelais
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PointRelais
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="adressePointRelais", type="string", length=255)
     */
    private $adressePointRelais;

    /**
     * @var string
     *
     * @ORM\Column(name="CPPointRelais", type="string", length=255)
     */
    private $cPPointRelais;

    /**
     * @var string
     *
     * @ORM\Column(name="VillePointRelais", type="string", length=255)
     */
    private $villePointRelais;

    /**
     * @var string
     *
     * @ORM\Column(name="latitudePointRelais", type="decimal", precision=10, scale=5)
     */
    private $latitudePointRelais;

    /**
     * @var string
     *
     * @ORM\Column(name="longitudePointRelais", type="decimal", precision=10, scale=5)
     */
    private $longitudePointRelais;

    /**
     * @var ModeLivraison
     *
     * @ORM\ManyToOne(targetEntity="BaB\CoreBundle\Entity\ModeLivraison")
     */
    private $modeLivraison;

    /**
     * @var string
     *
     * @ORM\Column(name="coutUnitaire", type="decimal", precision=10, scale=5)
     */
    private $coutUnitaire;

    /**
    * @var string
    * @ORM\Column(name="horraires", type="text", nullable=true)
    **/
    private $horrairesOuverture;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return PointRelais
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set adressePointRelais
     *
     * @param string $adressePointRelais
     * @return PointRelais
     */
    public function setAdressePointRelais($adressePointRelais)
    {
        $this->adressePointRelais = $adressePointRelais;

        return $this;
    }

    /**
     * Get adressePointRelais
     *
     * @return string 
     */
    public function getAdressePointRelais()
    {
        return $this->adressePointRelais;
    }

    /**
     * Set cPPointRelais
     *
     * @param string $cPPointRelais
     * @return PointRelais
     */
    public function setCPPointRelais($cPPointRelais)
    {
        $this->cPPointRelais = $cPPointRelais;

        return $this;
    }

    /**
     * Get cPPointRelais
     *
     * @return string 
     */
    public function getCPPointRelais()
    {
        return $this->cPPointRelais;
    }

    /**
     * Set villePointRelais
     *
     * @param string $villePointRelais
     * @return PointRelais
     */
    public function setVillePointRelais($villePointRelais)
    {
        $this->villePointRelais = $villePointRelais;

        return $this;
    }

    /**
     * Get villePointRelais
     *
     * @return string 
     */
    public function getVillePointRelais()
    {
        return $this->villePointRelais;
    }

    /**
     * Set latitudePointRelais
     *
     * @param string $latitudePointRelais
     * @return PointRelais
     */
    public function setLatitudePointRelais($latitudePointRelais)
    {
        $this->latitudePointRelais = $latitudePointRelais;

        return $this;
    }

    /**
     * Get latitudePointRelais
     *
     * @return string 
     */
    public function getLatitudePointRelais()
    {
        return $this->latitudePointRelais;
    }

    /**
     * Set longitudePointRelais
     *
     * @param string $longitudePointRelais
     * @return PointRelais
     */
    public function setLongitudePointRelais($longitudePointRelais)
    {
        $this->longitudePointRelais = $longitudePointRelais;

        return $this;
    }

    /**
     * Get longitudePointRelais
     *
     * @return string 
     */
    public function getLongitudePointRelais()
    {
        return $this->longitudePointRelais;
    }


    /**
    * Set horrairesOuverture
    * 
    * @param string $horrairesOuverture
    * @return PointRelais
    **/
    public function setHorrairesOuverture($horrairesOuverture) {
        $this->horrairesOuverture = $horrairesOuverture;

        return this;
    }

    /**
    * Get horrairesOuverture
    * @return string
    */
    public function getHorrairesOuverture() {
        return $this->horrairesOuverture;
    }


    function __toString()
    {
        return $this->getVillePointRelais()." - ".$this->getLibelle();
    }


}
