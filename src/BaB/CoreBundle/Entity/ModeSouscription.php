<?php

namespace BaB\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModeSouscription
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="BaB\CoreBundle\Entity\ModeSouscriptionRepository")
 */
class ModeSouscription
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="duree", type="integer")
     */
    private $duree;

    /**
     * @var boolean
     *
     * @ORM\Column(name="abonnement", type="string", length=255)
     */
    private $abonnement;

    /**
     * @var float
     *
     * @ORM\Column(name="prix_de_vente", type="decimal", precision=10, scale=2)
     */
    private $prixDeVente;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return ModeSouscription
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set duree
     *
     * @param integer $duree
     * @return ModeSouscription
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return integer 
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set abonnement
     *
     * @param boolean $abonnement
     * @return ModeSouscription
     */
    public function setAbonnement($abonnement)
    {
        $this->abonnement = $abonnement;

        return $this;
    }

    /**
     * Get abonnement
     *
     * @return boolean 
     */
    public function getAbonnement()
    {
        return $this->abonnement;
    }

    /**
     * @return float
     */
    public function getPrixDeVente()
    {
        return $this->prixDeVente;
    }

    /**
     * @param float $prixDeVente
     */
    public function setPrixDeVente($prixDeVente)
    {
        $this->prixDeVente = $prixDeVente;
    }



    function __toString()
    {
        return $this->getLibelle()." - ".$this->getPrixDeVente()."€";
    }


}
