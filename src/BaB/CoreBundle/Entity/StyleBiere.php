<?php

namespace BaB\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StyleBiere
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class StyleBiere
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomStyleBiere", type="string", length=255)
     */
    private $nomStyleBiere;

    /**
     * @var integer
     */
    private $nombreBieres;

    /**
     * @var Biere[]
     */
    private $bieres;

    /**
     * StyleBiere constructor.
     */
    public function __construct()
    {
        $this->setBieres(array());
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomStyleBiere
     *
     * @param string $nomStyleBiere
     * @return StyleBiere
     */
    public function setNomStyleBiere($nomStyleBiere)
    {
        $this->nomStyleBiere = $nomStyleBiere;

        return $this;
    }

    /**
     * Get nomStyleBiere
     *
     * @return string 
     */
    public function getNomStyleBiere()
    {
        return $this->nomStyleBiere;
    }

    /**
     * @return int
     */
    public function getNombreBieres()
    {
        return $this->nombreBieres;
    }

    /**
     * @param int $nombreBieres
     */
    public function setNombreBieres($nombreBieres)
    {
        $this->nombreBieres = $nombreBieres;
    }

    /**
     * @return Biere[]
     */
    public function getBieres()
    {
        return $this->bieres;
    }

    /**
     * @param Biere[] $bieres
     */
    public function setBieres($bieres)
    {
        $this->bieres = $bieres;
    }

    function __toString()
    {
        return $this->getNomStyleBiere();
    }
}
