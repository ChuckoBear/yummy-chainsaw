<?php
/**
 * Created by IntelliJ IDEA.
 * User: chuck
 * Date: 27/11/15
 * Time: 08:04
 */

namespace BaB\CoreBundle\Entity;


use Doctrine\ORM\EntityRepository;

class SouscriptionDetailRepository extends EntityRepository
{
    /**
     * @param $souscription Souscription L'identifiant de la souscription
     * @return SouscriptionDetail[] La liste des boxs liés à la souscription
     */
    public function findBySouscription($souscription) {
        return $this->findBy(array(
            'souscription' => $souscription
        ));
    }
}