<?php

namespace BaB\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Biere
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Biere
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomBiere", type="string", length=255)
     */
    private $nomBiere;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enVente", type="boolean")
     */
    private $enVente;

    /**
     * @var float
     *
     * @ORM\Column(name="tauxAlcoolBiere", type="decimal", precision=3, scale=1)
     */
    private $tauxAlcool;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;
    /**
     * @var string
     * @Assert\Url
     * @ORM\Column(name="lienUntappd", type="string", length=255, nullable=true)
     */
    private $lienUntappd;

    /**
     * @var string
     * @Assert\Url
     * @ORM\Column(name="lienRateBeer", type="string", length=255, nullable=true)
     */
    private $lienRateBeer;

    /**
     * @var StyleBiere
     * @ORM\ManyToOne(targetEntity="StyleBiere")
     * @ORM\JoinColumn(nullable=false)
     */
    private $style;


    /**
     * Brasseur principal de la bière
     * @var Brasserie
     * @ORM\ManyToOne(targetEntity="Brasserie")
     * @ORM\JoinColumn(nullable=false)
     */
    private $brasseur1;

    /**
     * Brasseur secondaire, s'il y en a un
     * @var Brasserie
     * @ORM\ManyToOne(targetEntity="Brasserie")
     * @ORM\JoinColumn(nullable=true)
     */
    private $brasseur2;

    /**
     * Brasseur tertiaire, s'il y en a un
     * @var Brasserie
     * @ORM\ManyToOne(targetEntity="Brasserie")
     * @ORM\JoinColumn(nullable=true)
     */
    private $brasseur3;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomBiere
     *
     * @param string $nomBiere
     * @return Biere
     */
    public function setNomBiere($nomBiere)
    {
        $this->nomBiere = $nomBiere;

        return $this;
    }

    /**
     * Get nomBiere
     *
     * @return string 
     */
    public function getNomBiere()
    {
        return $this->nomBiere;
    }



    /**
     * Set enVente
     *
     * @param boolean $enVente
     * @return Biere
     */
    public function setEnVente($enVente)
    {
        $this->enVente = $enVente;

        return $this;
    }

    /**
     * Get enVente
     *
     * @return boolean 
     */
    public function getEnVente()
    {
        return $this->enVente;
    }

    /**
     * @return mixed
     */
    public function getTauxAlcool()
    {
        return $this->tauxAlcool;
    }

    /**
     * @param mixed $tauxAlcool
     */
    public function setTauxAlcool($tauxAlcool)
    {
        $this->tauxAlcool = $tauxAlcool;
    }

    /**
     * @return mixed
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param mixed $commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }



    /**
     * @return string
     */
    public function getLienUntappd()
    {
        return $this->lienUntappd;
    }

    /**
     * @param string $lienUntappd
     */
    public function setLienUntappd($lienUntappd)
    {
        $this->lienUntappd = $lienUntappd;
    }

    /**
     * @return string
     */
    public function getLienRateBeer()
    {
        return $this->lienRateBeer;
    }

    /**
     * @param string $lienRateBeer
     */
    public function setLienRateBeer($lienRateBeer)
    {
        $this->lienRateBeer = $lienRateBeer;
    }



    /**
     * @return StyleBiere
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * @param StyleBiere $style
     */
    public function setStyle($style)
    {
        $this->style = $style;
    }

    /**
     * @return Brasserie
     */
    public function getBrasseur1()
    {
        return $this->brasseur1;
    }

    /**
     * @param Brasserie $brasseur1
     */
    public function setBrasseur1($brasseur1)
    {
        $this->brasseur1 = $brasseur1;
    }

    /**
     * @return Brasserie
     */
    public function getBrasseur2()
    {
        return $this->brasseur2;
    }

    /**
     * @param Brasserie $brasseur2
     */
    public function setBrasseur2($brasseur2)
    {
        $this->brasseur2 = $brasseur2;
    }

    /**
     * @return Brasserie
     */
    public function getBrasseur3()
    {
        return $this->brasseur3;
    }

    /**
     * @param Brasserie $brasseur3
     */
    public function setBrasseur3($brasseur3)
    {
        $this->brasseur3 = $brasseur3;
    }

    function __toString()
    {
        return $this->nomBiere;
    }


}
