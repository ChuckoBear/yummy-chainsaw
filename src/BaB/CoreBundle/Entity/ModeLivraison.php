<?php

namespace BaB\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModeLivraison
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ModeLivraison
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="prestataire", type="string", length=255)
     */
    private $prestataire;

    /**
     * @var float
     *
     * @ORM\Column(name="prix_de_vente", type="decimal", precision=10, scale=2)
     */
    private $prixDeVente;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return ModeLivraison
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set prestataire
     *
     * @param string $prestataire
     * @return ModeLivraison
     */
    public function setPrestataire($prestataire)
    {
        $this->prestataire = $prestataire;

        return $this;
    }

    /**
     * Get prestataire
     *
     * @return string 
     */
    public function getPrestataire()
    {
        return $this->prestataire;
    }

    /**
     * Set prixDeVente
     *
     * @param float $prixDeVente
     * @return ModeLivraison
     */
    public function setPrixDeVente($prixDeVente)
    {
        $this->prixDeVente = $prixDeVente;

        return $this;
    }

    /**
     * Get prixDeVente
     *
     * @return int
     */
    public function getPrixDeVente()
    {
        return $this->prixDeVente;
    }

    function __toString()
    {
        return $this->getLibelle()." - ".$this->getPrixDeVente()."€";
    }




}
