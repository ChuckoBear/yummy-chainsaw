<?php
/**
 * Created by IntelliJ IDEA.
 * User: chuck
 * Date: 25/11/15
 * Time: 12:29
 */

namespace BaB\FrontEndBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ValidationSouscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $label_attr = array('class'=>'col-md-6 control-label');
        $builder
            ->add('validationCGV', 'checkbox', array(
                'required' => true,
                'label' => "En cliquant là dessus, je certfie que toutes les informations entrées sont exactes et j'accepte les Conditions Générales de Vente"
            ));
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'bab_corebundle_validation_souscription';    }
}