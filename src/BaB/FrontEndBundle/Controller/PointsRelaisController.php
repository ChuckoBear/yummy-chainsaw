<?php

namespace BaB\FrontEndBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PointsRelaisController extends Controller
{
    public function showAction () {
        $em = $this->getDoctrine()->getManager();
        $pointsRelaisRepository = $em->getRepository('BaBCoreBundle:PointRelais');
        $pointsRelais = $pointsRelaisRepository->findAll();
        return $this->render('BaBFrontEndBundle:Default:points_relais.html.twig', array(
            'pointsRelais' => $pointsRelais
        ));
    }
}
