<?php

namespace BaB\FrontEndBundle\Controller;

use BaB\CoreBundle\Entity\Paypal;
use BaB\CoreBundle\Entity\Souscription;
use BaB\CoreBundle\Entity\SouscriptionDetail;
use BaB\FrontEndBundle\Form\SouscriptionType;
use BaB\FrontEndBundle\Form\ValidationSouscriptionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Session\Session;

class TunnelController extends Controller
{
    public function choixOffreAction($mode)
    {
        $souscription = new Souscription();
        $form = $this->get('form.factory')->create(new SouscriptionType(), $souscription);
        $pointsRelais = $this->getDoctrine()->getManager()->getRepository('BaBCoreBundle:PointRelais')->findAll();
        return $this->render('BaBFrontEndBundle:Tunnel:choixOffre.html.twig',
            array(
                'form' => $form->createView(),
                'mode' => $mode,
                'pointsRelais' => $pointsRelais
            ));
    }

    public function validationChoixOffreAction(Request $request)
    {
        $souscription = new Souscription();

        $form = $this->get('form.factory')->create(new SouscriptionType(), $souscription);
        if ($form->handleRequest($request)->isValid()) {
            if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
                $em = $this->getDoctrine()->getManager();

                if (0 < $souscription->getModeLivraison()->getPrixDeVente()) {
                    $souscription->setPointRelais(null);
                    $addressFact = $souscription->getAddressFacturation();
                    $addressFact->setUser($this->getUser());
                    $souscription->getAddressFacturation()->setUser($this->getUser());
                    $souscription->getAddressLivraison()->setUser($this->getUser());
                    $em->persist($souscription->getAddressFacturation());
                    $em->persist($souscription->getAddressLivraison());
                    $em->flush();
                } else {
                    $souscription->setAddressFacturation(null);
                    $souscription->setAddressLivraison(null);
                }
                $souscription->setPrixTotal(
                    $souscription->getModeLivraison()->getPrixDeVente() *
                    $souscription->getModeSouscription()->getDuree() +
                    $souscription->getModeSouscription()->getPrixDeVente());
                $souscription->setUser($this->getUser());
                $souscription->setDebutSouscriptionDate(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($souscription);

                $boxs = $em->getRepository('BaBCoreBundle:Box')
                    ->findBoxSouscripByDate(
                        $souscription->getBox()->getDateSortieBox(),
                        $souscription->getModeSouscription()->getDuree()
                    );
                foreach ($boxs as $box) {
                    $souscriptionDetail = new SouscriptionDetail();
                    $souscriptionDetail->setSouscription($souscription);
                    $souscriptionDetail->setBox($box);
                    $em->persist($souscriptionDetail);
                }
                $em->flush();
                //TODO: Definir la route et la page de rendu pour les commandes validées


                $souscriptionsDetail = $em->getRepository('BaBCoreBundle:SouscriptionDetail')
                    ->findBy(array('souscription' => $souscription));

                $formValid = $this->get('form.factory')->create(new ValidationSouscriptionType(), null, array(
                    'action' => $this->generateUrl('demande_paiement')
                ));
                $address = null != $souscription->getAddressFacturation();
                return $this->render('@BaBFrontEnd/Tunnel/recapCommande.html.twig', array(
                    'souscription' => $souscription,
                    'detailsSouscription' => $souscriptionsDetail,
                    'address' => $address,
                    'form' => $formValid->createView()

                ));
            }
            return $this->redirectToRoute('fos_user_security_login');


        }
        return $this->redirectToRoute('achat', array('mode' => "acheter"));

    }


    public function authentificationAction(Souscription $souscription)
    {

    }

    public function panierAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $souscriptionRepository = $em->getRepository('BaBCoreBundle:Souscription');


            $user = $this->getUser();
            $souscriptions = $souscriptionRepository->findBy(
                array(
                    'user' => $user,
                ),
                array(
                    'debutSouscriptionDate' => 'DESC'
                ),
                1);
            if (null == $souscriptions) {
                throw $this->createNotFoundException("Vous n'avez pas de panier enregistré.");
            }
            $souscription = $souscriptions[0];

            $souscriptionsDetail = $em->getRepository('BaBCoreBundle:SouscriptionDetail')
                ->findBy(array('souscription' => $souscription));


            $formValid = $this->get('form.factory')->create(new ValidationSouscriptionType(), null, array(
                'action' => $this->generateUrl('demande_paiement')
            ));
            $address = null != $souscription->getAddressFacturation();

            return $this->render('@BaBFrontEnd/Tunnel/recapCommande.html.twig', array(
                'souscription' => $souscription,
                'detailsSouscription' => $souscriptionsDetail,
                'address' => $address,
                'form' => $formValid->createView()
            ));
        }
        throw $this->createAccessDeniedException("Vous n'avez pas accès à ce panier");

    }

    public function prepareAction(Request $request)
    {

        $form = $this->get('form.factory')->create(new ValidationSouscriptionType());

        if ($form->handleRequest($request)->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $souscriptionRepository = $em->getRepository('BaBCoreBundle:Souscription');


            $user = $this->getUser();
            $souscriptions = $souscriptionRepository->findBy(
                array(
                    'user' => $user,
                ),
                array(
                    'debutSouscriptionDate' => 'DESC'
                ),
                1);
            $souscription = $souscriptions[0];


            $products = array(
                "box" => array(
                    "name" => $souscription->getModeSouscription()->getLibelle(),
                    "priceTVA" => $souscription->getModeSouscription()->getPrixDeVente(),
                    "count" => 1
                ),
                "ship" => array(
                    "price" => $souscription->getModeLivraison()->getPrixDeVente() * $souscription->getModeSouscription()->getDuree()
                ),
                "total" => $souscription->getPrixTotal()
            );


            $paramsPayment = array(
                "LANDINGPAGE" => "Billing",

                //Données de livraison
                "PAYMENTREQUEST_0_SHIPTONAME" => $souscription->getUser()->getFirstName() . " " . $souscription->getUser()->getLastName(),

                //Callback
                "RETURNURL" => $this->generateUrl('paiement_effectue', array(), true),
                "CANCELURL" => $this->generateUrl('panier', array(), true),//TODO

                //Devise
                "PAYMENTREQUEST_0_CURRENCYCODE" => "EUR",

                //Produits
                "L_PAYMENTREQUEST_0_NAME0" => $products['box']['name'],
                "L_PAYMENTREQUEST_0_QTY0" => $products['box']['count'],
                "L_PAYMENTREQUEST_0_AMT0" => $souscription->getModeSouscription()->getPrixDeVente(),
                "L_PAYMENTREQUEST_n_TAXAMT" => $souscription->getModeSouscription()->getPrixDeVente() - $souscription->getModeSouscription()->getPrixDeVente() / 1.2,

                //Total
                "PAYMENTREQUEST_0_ITEMAMT" => $souscription->getModeSouscription()->getPrixDeVente(),
                "PAYMENTREQUEST_0_SHIPPINGAMT" => $products['ship']['price'],
                "PAYMENTREQUEST_0_AMT" => $products['total'],
                "PAYMENTREQUEST_n_TAXAMT" => $products['total'] - $products['total'] / 1.2,

                "PAYMENTREQUEST_0_PAYMENTACTION" => "Sale",
                "NOTETOBUYER" => "",
                "PAYMENTREQUEST_n_DESC" => $products['box']['name'],

            );

            if (null == $souscription->getPointRelais()) {
                $paramsPayment["PAYMENTREQUEST_0_SHIPTOSTREET"] =
                    $souscription->getAddressLivraison()->getNumeroAdresse() . " " . $souscription->getAddressLivraison()->getNomVoieAdresse();
                $paramsPayment["PAYMENTREQUEST_0_SHIPTOSTREET2"] =
                    $souscription->getAddressLivraison()->getComplementAdresse();
                $paramsPayment["PAYMENTREQUEST_0_SHIPTOCITY"] =
                    $souscription->getAddressLivraison()->getVilleAdresse();
                $paramsPayment["PAYMENTREQUEST_0_SHIPTOSTATE"] =
                    $souscription->getAddressLivraison()->getPaysAdresse();
                $paramsPayment["PAYMENTREQUEST_0_SHIPTOZIP"] =
                    $souscription->getAddressLivraison()->getCpAdresse();
                $paramsPayment["PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"] = "FR";
            } else {
                $paramsPayment["PAYMENTREQUEST_0_SHIPTOSTREET"] =
                    $souscription->getPointRelais()->getLibelle();
                $paramsPayment["PAYMENTREQUEST_0_SHIPTOCITY"] =
                    $souscription->getPointRelais()->getVillePointRelais();
                $paramsPayment["PAYMENTREQUEST_0_SHIPTOSTATE"] =
                    "France";
                $paramsPayment["PAYMENTREQUEST_0_SHIPTOZIP"] =
                    $souscription->getPointRelais()->getCPPointRelais();
                $paramsPayment["PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"] =
                    "FR";
            }
            $paypal = new Paypal(false, false, false, 'prod' == $this->container->getParameter('kernel.environment'));
            $response = $paypal->request("SetExpressCheckout", $paramsPayment);

            if ($response) {
                $urlBase = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&useraction=commit&token=";
                if ('prod' == $this->container->getParameter('kernel.environment'))
                    $urlBase = str_replace('sandbox.', '', $urlBase);
                $urlToken = $urlBase . $response['TOKEN'];
                $souscription->setEtatCommande($em->getRepository('BaBCoreBundle:EtatCommande')->find(1));
                $em->persist($souscription);
                $em->flush();
                return $this->redirect($urlToken);
            } else {
//                var_dump($paypal->getErrors());
                return $this->render('@BaBFrontEnd/Tunnel/erreurPaiement.html.twig', array(
                    'request' => $request,
                    'response' => $response,
                    'token' => $request->query,
                    'errors' => $paypal->getErrors(),
                    'paramsPayment' => $paramsPayment
                ));
                //TODO; Generer un rendu en cas d'erreur
            }
        }
    }

    public function captureDoneAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $souscriptionRepository = $em->getRepository('BaBCoreBundle:Souscription');


        $user = $this->getUser();
        $souscriptions = $souscriptionRepository->findBy(
            array(
                'user' => $user,
            ),
            array(
                'debutSouscriptionDate' => 'DESC'
            ),
            1);
        $souscription = $souscriptions[0];
        $souscriptionsDetail = $em->getRepository('BaBCoreBundle:SouscriptionDetail')
            ->findBy(array('souscription' => $souscription));

        $products = array(
            "box" => array(
                "name" => $souscription->getModeSouscription()->getLibelle(),
                "priceTVA" => $souscription->getModeSouscription()->getPrixDeVente(),
                "count" => 1
            ),
            "ship" => array(
                "price" => $souscription->getModeLivraison()->getPrixDeVente() * $souscription->getModeSouscription()->getDuree()
            ),
            "total" => $souscription->getPrixTotal()
        );


        $paramsPaypal = array(
            "TOKEN" => $request->query->get('token')
        );

        $paypal = new Paypal(false, false, false, 'prod' == $this->container->getParameter('kernel.environment'));
        $response = $paypal->request("GetExpressCheckoutDetails", $paramsPaypal);

        if ($response) {
            $souscription->setEtatPaypal($response['CHECKOUTSTATUS']);

            //Tout s'est bien passé
            if ($response['CHECKOUTSTATUS'] == "PaymentActionCompleted") {
                return $this->render('@BaBFrontEnd/Tunnel/erreurPaiement.html.twig', array(
                    'message' => "Le paiement a déjà été validé."
                ));
            }

        } else {
            //Il y a eu une erreur dans la requete
//            var_dump($paypal->getErrors());
            //TODO; Generer un rendu en cas d'erreur
        }
        $paramsPaypal = array(
            //Produits
            "L_PAYMENTREQUEST_0_NAME0" => $products['box']['name'],
            "L_PAYMENTREQUEST_0_QTY0" => $products['box']['count'],
            "L_PAYMENTREQUEST_0_AMT0" => $souscription->getModeSouscription()->getPrixDeVente(),
//            "L_PAYMENTREQUEST_n_TAXAMT" => $souscription->getModeSouscription()->getPrixDeVente()/1.2

            //Total
            "PAYMENTREQUEST_0_ITEMAMT" => $souscription->getModeSouscription()->getPrixDeVente(),
            "PAYMENTREQUEST_0_SHIPPINGAMT" => $products['ship']['price'],
            "PAYMENTREQUEST_0_AMT" => $products['total'],
            "PAYMENTREQUEST_n_TAXAMT" => $products['total'] - $products['total'] / 1.2,

            "TOKEN" => $request->query->get('token'),
            "PAYERID" => $request->query->get('PayerID'),
            'PAYMENTACTION' => "Sale",
            "PAYMENTREQUEST_0_CURRENCYCODE" => "EUR"

        );
        $response = $paypal->request('DoExpressCheckoutPayment', $paramsPaypal);

        if ($response) {
            //Tout s'est bien passé
            $souscription->setTransactionId($response['PAYMENTINFO_0_TRANSACTIONID']);
            $etatCommandeRepository = $em->getRepository('BaBCoreBundle:EtatCommande');
            $souscription->setEtatCommande($etatCommandeRepository->find(2));
            $responseGetExpress = $paypal->request("GetExpressCheckoutDetails", $paramsPaypal);
            $souscription->setEtatPaypal($responseGetExpress['CHECKOUTSTATUS']);

            $em->persist($souscription);
            $em->flush();
            return $this->render('@BaBFrontEnd/Tunnel/paiementValide.html.twig', array(
                'souscription' => $souscription,
                'detailsSouscription' => $souscriptionsDetail

            ));

        } else {
            //Il y a eu une erreur dans la requete
//            var_dump($paypal->getErrors());
            //TODO; Generer un rendu en cas d'erreur
            return $this->render('@BaBFrontEnd/Tunnel/erreurPaiement.html.twig', array(
                'request' => $request,
                'response' => $response,
                'token' => $request->query
            ));
        }


    }

}
