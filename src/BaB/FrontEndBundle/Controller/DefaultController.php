<?php

namespace BaB\FrontEndBundle\Controller;

use BaB\FrontEndBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    public function homeAction()
    {
        return $this->render('BaBCoreBundle:Default:index.html.twig');
    }

    public function buyAction($mode) {

        return $this->render('@BaBFrontEnd/Default/form_achat.html.twig', array(
            'mode' => $mode
        ));
    }

    /**
     * @\Symfony\Component\Routing\Annotation\Route(name="/contact")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function contactSubmitAction(Request $request) {
        $form = $this->createForm(new ContactType());
        $form->bind($request);
        if ($form->isValid()) {
            $message = \Swift_Message::newInstance()
                ->setSubject("[Formulaire Contact]".$form->get('subject')->getData())
                ->setFrom($form->get('email')->getData())
                ->setTo('contact@boiteabiere.fr')
                ->setBody(
                    $this->renderView('@BaBFrontEnd/Default/contact.html.twig'),
                    array(
                        'ip' => $request->getClientIp(),
                        'name' => $form->get('name')->getData(),
                        'message' => $form->get('message')->getData()
                    )
                );
            $this->get('mailer')->send($message);
            $request->getSession()->getFlashBag()->add('success', 'Your email has been sent! Thanks!');

        }
        return $this->redirect($this->generateUrl('home'));
    }

    public function contactAction() {
        $form = $this->createForm(new ContactType());
        return $this->render('@BaBFrontEnd/Default/contact.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    public function inaugurationAction() {
        return $this->render('@BaBFrontEnd/Default/inauguration.html.twig');
    }

    public function cgvAction() {
        return $this->render('@BaBFrontEnd/Default/cgv.html.twig');
    }
}
