<?php

namespace BaB\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends Controller
{
    public function addressListAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $addressRepository = $em->getRepository('BaBUserBundle:Address');

        $address = $addressRepository->findBy(array(
            'user' => $user
        ));
        return $this->render('@BaBUser/Profile/address-list.html.twig', array(
            'address' => $address
        ));
    }

    public function commandeListAction() {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();
        $etatCommande = $em->getRepository('BaBCoreBundle:EtatCommande')->findAll();
        $commandeRepository = $em->getRepository('BaBCoreBundle:Souscription');

        $commandes = $commandeRepository->findBy(array(
            'user' => $user,
            'etatCommande' => $etatCommande
        ));

        return $this->render('BaBUserBundle:Profile:commandeList.html.twig', array(
            'commandes' => $commandes
        ));
    }

    public function commandeDetailAction($id) {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();
        $etatCommande = $em->getRepository('BaBCoreBundle:EtatCommande')->findAll();
        $commandeRepository = $em->getRepository('BaBCoreBundle:Souscription');

        $commande = $commandeRepository->find($id);

        if(null == $commande) {
//           return new NotFoundHttpException("Cette commande n'existe pas.");
            throw $this->createNotFoundException("Cette commande n'existe pas.");
        }

        if ($user != $commande->getUser() || !in_array($commande->getEtatCommande(), $etatCommande)) {
//            return new AccessDeniedHttpException("Vous n'avez pas accès à cette commande");
            throw $this->createAccessDeniedException("Vous n'avez pas accès à cette commande");
        }

        $commandeDetails = $em->getRepository('BaBCoreBundle:SouscriptionDetail')->findBy(
            array(
                'souscription' => $commande
            )
        );

        $address = null != $commande->getAddressFacturation();

        return $this->render('BaBUserBundle:Profile:commandeDetail.html.twig', array(
            'commande' => $commande,
            'detailsCommande' => $commandeDetails,
            'adress' => $address
        ));
    }
}
