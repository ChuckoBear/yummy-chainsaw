<?php

namespace BaB\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Address
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Address
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroAdresse", type="string", length=255)
     */
    private $numeroAdresse;

    /**
     * @var string
     *
     * @ORM\Column(name="nomVoieAdresse", type="string", length=255)
     */
    private $nomVoieAdresse;

    /**
     * @var string
     *
     * @ORM\Column(name="complementAdresse", type="string", length=255)
     */
    private $complementAdresse;

    /**
     * @var string
     *
     * @ORM\Column(name="cpAdresse", type="string", length=255)
     */
    private $cpAdresse;

    /**
     * @var string
     *
     * @ORM\Column(name="villeAdresse", type="string", length=255)
     */
    private $villeAdresse;

    /**
     * @var string
     *
     * @ORM\Column(name="paysAdresse", type="string", length=255)
     */
    private $paysAdresse;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="BaB\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Address
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set numeroAdresse
     *
     * @param string $numeroAdresse
     * @return Address
     */
    public function setNumeroAdresse($numeroAdresse)
    {
        $this->numeroAdresse = $numeroAdresse;

        return $this;
    }

    /**
     * Get numeroAdresse
     *
     * @return string 
     */
    public function getNumeroAdresse()
    {
        return $this->numeroAdresse;
    }

    /**
     * Set nomVoieAdresse
     *
     * @param string $nomVoieAdresse
     * @return Address
     */
    public function setNomVoieAdresse($nomVoieAdresse)
    {
        $this->nomVoieAdresse = $nomVoieAdresse;

        return $this;
    }

    /**
     * Get nomVoieAdresse
     *
     * @return string 
     */
    public function getNomVoieAdresse()
    {
        return $this->nomVoieAdresse;
    }

    /**
     * Set complementAdresse
     *
     * @param string $complementAdresse
     * @return Address
     */
    public function setComplementAdresse($complementAdresse)
    {
        $this->complementAdresse = $complementAdresse;

        return $this;
    }

    /**
     * Get complementAdresse
     *
     * @return string 
     */
    public function getComplementAdresse()
    {
        return $this->complementAdresse;
    }

    /**
     * Set cpAdresse
     *
     * @param string $cpAdresse
     * @return Address
     */
    public function setCpAdresse($cpAdresse)
    {
        $this->cpAdresse = $cpAdresse;

        return $this;
    }

    /**
     * Get cpAdresse
     *
     * @return string 
     */
    public function getCpAdresse()
    {
        return $this->cpAdresse;
    }

    /**
     * Set villeAdresse
     *
     * @param string $villeAdresse
     * @return Address
     */
    public function setVilleAdresse($villeAdresse)
    {
        $this->villeAdresse = $villeAdresse;

        return $this;
    }

    /**
     * Get villeAdresse
     *
     * @return string 
     */
    public function getVilleAdresse()
    {
        return $this->villeAdresse;
    }

    /**
     * Set paysAdresse
     *
     * @param string $paysAdresse
     * @return Address
     */
    public function setPaysAdresse($paysAdresse)
    {
        $this->paysAdresse = $paysAdresse;

        return $this;
    }

    /**
     * Get paysAdresse
     *
     * @return string 
     */
    public function getPaysAdresse()
    {
        return $this->paysAdresse;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


}
