<?php

namespace BaB\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddressType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $label_attr = array('class'=>'col-md-6 control-label');

        $builder
            ->add('libelle', 'text', array(
                'label' => 'Libelle',
                'label_attr' => $label_attr
            ))
            ->add('numeroAdresse','text', array(
                'label' => 'Numero ',
                'label_attr' => $label_attr
            ))
            ->add('nomVoieAdresse','text', array(
                'label' => 'Nom de voie',
                'label_attr' => $label_attr
            ))
            ->add('complementAdresse','text', array(
                'label' => 'Complément d\'adresse',
                'label_attr' => $label_attr
            ))
            ->add('cpAdresse','text', array(
                'label' => 'Code postal',
                'label_attr' => $label_attr
            ))
            ->add('villeAdresse','text', array(
                'label' => 'Ville',
                'label_attr' => $label_attr
            ))
            ->add('paysAdresse','text', array(
                'label' => 'Pays',
                'label_attr' => $label_attr
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BaB\UserBundle\Entity\Address'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bab_userbundle_address';
    }
}
