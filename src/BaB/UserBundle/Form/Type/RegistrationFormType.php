<?php

namespace BaB\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', array(
                'label'                 => 'Email : ',
                'translation_domain'    => 'FOSUserBundle'))
            ->add('username', null, array(
                'label'                 => 'Pseudo : ',
                'translation_domain'    => 'FOSUserBundle'))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'Les mots de passes doivent correspondre',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options'  => array('label' => 'Mot de passe'),
                'second_options' => array('label' => 'Confirmez le mot de passe'),
//                'translation_domain'    => 'FOSUserBundle',
//                'label'                 => 'Mot de passe : ',

            ))
        ;

        $builder
            ->add('firstName', null, array(
                'label'                     => 'Prenom de la personne',
                'translation_domain'        => 'FOSUserBundle'))
            ->add('lastName', null, array(
                'label' => 'Nom de la personne',
                'translation_domain' => 'FOSUserBundle'))
            ->add('newsletter', 'checkbox', array(
                'label' => 'Souhaitez-vous recevoir notre newsletter ?'
            ));
    }

    public function getParent()
    {
        return 'fos_user_registration';
    }

    public function getName()
    {
        return 'bab_user_registration';
    }
}