<?php
/**
 * Created by IntelliJ IDEA.
 * User: chuck
 * Date: 26/10/15
 * Time: 17:33
 */

namespace BaB\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginFormType extends AbstractType
{



    public function getParent() {
        return 'fos_user_security_login';
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'bab_user_login';
    }
}