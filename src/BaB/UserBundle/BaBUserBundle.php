<?php

namespace BaB\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BaBUserBundle extends Bundle
{
    public function getParent() {
        return 'FOSUserBundle';
    }
}
