<?php

namespace BaB\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BrasserieType extends AbstractType
{



    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $label_attr = array('class'=>'col-sm-3 control-label');


        $builder
            ->add('nomBrasserie', 'text', array(
                'label' => 'Nom de la brasserie',
                'label_attr'=>$label_attr))
            ->add('adresse', 'text', array(
                'required' => false,
                'label' => 'Adresse ',
                'label_attr'=>$label_attr))
            ->add('cpBrasserie', 'text', array(
                'label' => 'Code postal',
                'label_attr'=>$label_attr))
            ->add('villeBrasserie', 'text', array(
                'label' => 'Ville',
                'label_attr'=>$label_attr))
            ->add('paysBrasserie', 'country', array(
                'label' => 'Pays',
                'label_attr'=>$label_attr))
            ->add('telephoneBrasserie', null, array(
                'required' => false,
                'label' => 'Téléphone de la brasserie',
                'label_attr'=>$label_attr))
            ->add('emailBrasserie', 'email',array(
                'required' => false,
                'label' => 'Email',
                'label_attr'=>$label_attr))
            ->add('siteWebBrasserie', null, array(
                'required' => false,
                'label' => 'Site web',
                'label_attr'=>$label_attr))
            ->add('dateCreationBrasserie', 'date', array(
                'required' => false,
                'label' => 'Date de création',
                'label_attr'=>$label_attr))
            ->add('nombreEmployeesBrasserie', 'integer', array(
                'required' => false,
                'label' => 'Nombre d\'employees',
                'label_attr'=>$label_attr))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BaB\CoreBundle\Entity\Brasserie'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bab_corebundle_brasserie';
    }
}
