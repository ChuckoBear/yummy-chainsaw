<?php

namespace BaB\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ModeSouscriptionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $label_attr = array('class'=>'col-sm-3 control-label');
        $builder
            ->add('libelle', 'text', array(
                'label'         => 'Libellé du mode de souscription',
                'label_attr'    => $label_attr
            ))
            ->add('duree','integer', array(
                'label'         => 'Durée de la souscription',
                'label_attr'    => $label_attr
            ))
            ->add('abonnement', 'choice', array(
                'choices'       => array(
                    'abonnement'    => 'Abonnement',
                    'ponctuel'      => 'Achat ponctuel'
                ),
                'expanded'          => true,
                'multiple'          => false,
                'label'         => 'Abonnement',
                'label_attr'    => $label_attr
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BaB\CoreBundle\Entity\ModeSouscription'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bab_corebundle_modesouscription';
    }
}
