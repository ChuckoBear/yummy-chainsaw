<?php

namespace BaB\BackEndBundle\Form;

use BaB\UserBundle\Entity\Address;
use BaB\UserBundle\Form\AddressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SouscriptionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $label_attr = array('class'=>'col-md-6 control-label');
        $builder
            ->add('user', 'entity', array(
                'class' => 'BaB\UserBundle\Entity\User',
                'label' => "Choix de l'utilisateur",
                'label_attr' => $label_attr
            ))
            ->add('modeSouscription', 'entity', array(
                'class'         =>  'BaB\CoreBundle\Entity\ModeSouscription',
                'label'         =>  'Souhaitez vous prendre un abonnement ou faire un achat ponctuel ?',
                'label_attr'    =>  $label_attr,

            ))
            ->add('box', 'entity', array(
                'class'         =>  'BaB\CoreBundle\Entity\Box',
                'label'         =>  'Par quel mois souhaitez vous commencer ?',
                'label_attr'    =>  $label_attr,
                'query_builder' => function(\BaB\CoreBundle\Entity\BoxRepository $er) {
                    return $er->findBoxAvalaible();
                }
            ))
            ->add('modeLivraison', 'entity', array(
                'class'         =>  'BaB\CoreBundle\Entity\ModeLivraison',
                'label'         =>  'Quel mode de livraison souhaitez vous ?',
                'label_attr'    =>  $label_attr,


            ))
            ->add('etatCommande', 'entity', array(
                'class' => 'BaB\CoreBundle\Entity\EtatCommande',
                'label' => 'Dans quel état est la commande ?',
                'label_attr' => $label_attr
            ))
            ->add('commentaire', 'text', array(
                'label' => 'Commentaire',
                'label_attr' => $label_attr
            ))
            ->add('pointRelais', 'entity', array(
                'class' => 'BaB\CoreBundle\Entity\PointRelais',
                'label' => 'Point relais choisi',
                'label_attr' => $label_attr,
                'placeholder' => 'Choisissez un point relais',
                'required' => false
            ))
            ->add('addressLivraison', new AddressType() , array('required' => false))
            ->add('addressFacturation', new AddressType(), array('required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BaB\CoreBundle\Entity\Souscription'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bab_corebundle_souscription';
    }
}
