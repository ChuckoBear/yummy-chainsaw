<?php

namespace BaB\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BiereType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $label_attr = array('class'=>'col-sm-3 control-label');
        $builder
            ->add('nomBiere', 'text', array(
                'label'         => 'Dénomination commerciale de la bière',
                'label_attr'    => $label_attr
            ))
            ->add('enVente', 'checkbox', array(
                'label'         => 'Bière commercialisé',
                'label_attr'    => $label_attr
            ))
            ->add('tauxAlcool', 'number', array(
                'label'         => 'Taux d\'alcool',
                'precision'     => 1,
                'label_attr'    => $label_attr
            ))
            ->add('commentaire', 'textarea', array(
                'label'         => 'Commentaire de l\'équipe',
                'label_attr'    => $label_attr,
                'required'      => false,

            ))
            ->add('lienUntappd', null, array(
                'label'         => 'Lien vers la fiche Untappd',
                'label_attr'     => $label_attr,
                'required'      => false,
            ))
            ->add('lienRateBeer', null, array(
                'label'         => 'Lien vers la fiche Ratebeer',
                'label_attr'    => $label_attr,
                'required'      => false,
            ))
            ->add('style', 'entity', array(
                'class'         => 'BaB\CoreBundle\Entity\StyleBiere',
                'label'         => 'Style associé',
                'label_attr'    => $label_attr
            ))
            ->add('brasseur1', 'entity', array(
                'class'         => 'BaB\CoreBundle\Entity\Brasserie',
                'label'         => 'Brasserie associé',
                'label_attr'    => $label_attr
            ))
            ->add('brasseur2', 'entity', array(
                'class'         => 'BaB\CoreBundle\Entity\Brasserie',
                'empty_value'   =>'Aucune brasserie',
                'required'      => false,
                'label'         => 'Brasserie associé',
                'label_attr'    => $label_attr
            ))
            ->add('brasseur3', 'entity', array(
                'class'         => 'BaB\CoreBundle\Entity\Brasserie',
                'empty_value'   =>'Aucune brasserie',
                'required'      => false,
                'label'         => 'Brasserie associé',
                'label_attr'    => $label_attr
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BaB\CoreBundle\Entity\Biere'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bab_corebundle_biere';
    }
}
