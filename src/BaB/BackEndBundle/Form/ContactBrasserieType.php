<?php

namespace BaB\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactBrasserieType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $label_attr = array('class'=>'col-sm-3 control-label');
        $builder
            ->add('nomContact', 'text', array(
                'label' => 'Nom du contact', 'label_attr'=>$label_attr))
            ->add('prenomContact', 'text', array(
                'label' => 'Prénom du contact', 'label_attr'=>$label_attr))
            ->add('emailContact', 'email', array(
                'label' => 'Email', 'label_attr'=>$label_attr))
            ->add('telephoneContact', 'text', array(
                'label' => 'Téléphone', 'label_attr'=>$label_attr))
            ->add('posteContact', 'text', array(
                'label' => 'Poste dans la brasserie', 'label_attr'=>$label_attr))
            ->add('brasserie', 'entity',
                array(
                    'class' => 'BaB\CoreBundle\Entity\Brasserie',
                    'label' => 'Brasserie associé',
                    'label_attr'=>$label_attr
                ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BaB\CoreBundle\Entity\ContactBrasserie'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bab_corebundle_contactbrasserie';
    }
}
