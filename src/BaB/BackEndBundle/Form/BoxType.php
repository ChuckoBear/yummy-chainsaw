<?php

namespace BaB\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BoxType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $label_attr = array('class'=>'col-sm-3 control-label');
        $builder
            ->add('nomBox', 'text', array(
                'label'         => 'Dénomination de la box',
                'label_attr'    => $label_attr
            ))
            ->add('dateSortieBox', 'date', array(
                'label'         => 'Date de sortie de la box',
                'label_attr'    => $label_attr
            ))
            ->add('descriptionBox', 'textarea', array(
                'label'         => 'Description commerciale de la box',
                'label_attr'    => $label_attr
            ))
            ->add('nombreBoxProposes', 'integer', array(
                'label'         => 'Nombre de box proposés à la vente',
                'label_attr'    => $label_attr
            ))
            ->add('prixDeVenteHT', 'money', array(
                'label'         => 'Prix de vente HT de la box',
                'label_attr'    => $label_attr,
            ))
            ->add('dateDebutVenteBox', 'date', array(
                'label'         => 'Date de début de mise en vente de la box',
                'label_attr'    => $label_attr,
            ))
            ->add('dateFinVenteBox', 'money', array(
                'label'         => 'Date de fin de mise en vente de la box',
                'label_attr'    => $label_attr,
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BaB\CoreBundle\Entity\Box'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bab_corebundle_box';
    }
}
