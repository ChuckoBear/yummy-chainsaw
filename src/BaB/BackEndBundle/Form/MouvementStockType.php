<?php

namespace BaB\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MouvementStockType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $label_attr = array('class'=>'col-sm-3 control-label');

        $builder
            ->add('natureMouvementStock', 'text', array(
                'label'         => 'Libellé du mouvement de stock',
                'label_attr'    => $label_attr
            ))
            ->add('typeOperationStock', 'choice', array(
                'label'         => 'Entrée/Sortie',
                'choices'       => array(
                    'in'        => 'Entrée',
                    'out'       => 'Sortie'
                ),
                'label_attr'    => $label_attr
            ))
            ->add('quantiteMouvementStock', 'integer', array(
                'label'         => 'Quantité',
                'label_attr'    => $label_attr
            ))
            ->add('valeurUnitaire', 'money', array(
                'label'         => 'Valeur unitaire',
                'label_attr'    => $label_attr,
                
            ))
            ->add('biere', 'entity', array(
                'class'         => 'BaB\CoreBundle\Entity\Biere',
                'label'         => 'Bière',
                'label_attr'    => $label_attr
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BaB\CoreBundle\Entity\MouvementStock'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bab_corebundle_mouvementstock';
    }
}
