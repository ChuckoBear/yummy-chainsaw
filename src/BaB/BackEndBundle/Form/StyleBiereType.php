<?php

namespace BaB\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StyleBiereType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $label_attr = array('class'=>'col-sm-3 control-label');

        $builder
            ->add('nomStyleBiere', 'text', array(
                'label' => 'Nom du style de la bière', 'label_attr' => $label_attr
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BaB\CoreBundle\Entity\StyleBiere'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bab_corebundle_stylebiere';
    }
}
