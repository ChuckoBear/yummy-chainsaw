<?php

namespace BaB\BackEndBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $boxRepository = $em->getRepository('BaBCoreBundle:Box');
        $boxs = $boxRepository->findAll();
        foreach ($boxs as $box) {
            $box->setNombreBoxVendues(
            $em->getRepository('BaBCoreBundle:Box')->countBoxValidee($box));

        }
        return $this->render('@BaBBackEnd/Default/index.html.twig', array(
            'boxs' => $boxs
        ));
    }
}
