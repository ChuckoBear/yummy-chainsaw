<?php
/**
 * Created by IntelliJ IDEA.
 * User: Gecko
 * Date: 04/08/2015
 * Time: 12:04
 */

namespace BaB\BackEndBundle\Controller;


use BaB\CoreBundle\Entity\ContactBrasserie;
use BaB\BackEndBundle\Form\ContactBrasserieType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ContactBrasserieController extends Controller
{
    public function addAction(Request $request)
    {
        $contactBrasserie = new ContactBrasserie();

        $form = $this->get('form.factory')->create(new ContactBrasserieType(), $contactBrasserie);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contactBrasserie);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Brasserie bien enregistrée.');
            return $this->redirect($this->generateUrl('home'));
        }
        return $this->render('BaBBackEndBundle:ContactBrasserie:form.html.twig', array(
            'form'  => $form->createView()
        ));
    }
}