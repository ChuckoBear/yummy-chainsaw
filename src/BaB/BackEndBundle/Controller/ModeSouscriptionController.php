<?php

namespace BaB\BackEndBundle\Controller;

use BaB\CoreBundle\Entity\ModeSouscription;
use BaB\BackEndBundle\Form\ModeSouscriptionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ModeSouscriptionController extends Controller
{
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $mode_souscription_repository = $em->getRepository('BaBCoreBundle:ModeSouscription');
        $modes_souscription = $mode_souscription_repository->findAll();

        return $this->render('BaBBackEndBundle:ModeSouscription:list.html.twig', array(
            'modes_souscription' => $modes_souscription

        ));
    }

    public function addAction()
    {
        $mode_souscription = new ModeSouscription();

        $form = $this->get('form.factory')->create(new ModeSouscriptionType(), $mode_souscription);

        return $this->render('BaBBackEndBundle:ModeSouscription:add.html.twig', array(
                'form' => $form->createView()
            ));
    }

    public function addModeAction(Request $request) {
        $mode_souscription = new ModeSouscription();

        $form = $this->get('form.factory')->create(new ModeSouscriptionType(), $mode_souscription);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($mode_souscription);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Mode de souscription bien enregistrée.');
            return $this->redirect($this->generateUrl('mode_souscription_list'));
        }


    }
}
