<?php
/**
 * Created by IntelliJ IDEA.
 * User: Gecko
 * Date: 04/08/2015
 * Time: 16:24
 */

namespace BaB\BackEndBundle\Controller;


use BaB\CoreBundle\Entity\StyleBiere;
use BaB\BackEndBundle\Form\StyleBiereType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class StyleBiereController extends Controller
{

    public function addAction(Request $request)
    {
        $styleBiere = new StyleBiere();

        $form = $this->get('form.factory')->create(new StyleBiereType(), $styleBiere);


        return $this->render('BaBBackEndBundle:StyleBiere:form.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function addStyleAction(Request $request)
    {
        $styleBiere = new StyleBiere();

        $form = $this->get('form.factory')->create(new StyleBiereType(), $styleBiere);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($styleBiere);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Style bien enregistrée.');
            return $this->redirect($this->generateUrl('style_biere_list'));
        }
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $styleBiereRepository = $em->getRepository('BaBCoreBundle:StyleBiere');
        $styleBiere = $styleBiereRepository->find($id);
        if (null === $styleBiere) {
            throw new NotFoundHttpException("Le style de biere d'id" . $id . "n'existe pas.");
        }
        $listBieres = $em->getRepository('BaBCoreBundle:Biere')
            ->findBy(array(
                'style'         => $styleBiere
            ));
        $styleBiere->setBieres($listBieres);
        $styleBiere->setNombreBieres(count($styleBiere->getBieres()));
        $refString = $styleBiere->getNombreBieres()." référence";
        if ($styleBiere->getNombreBieres() > 1) {
            $refString = $refString."s";
        }
        return $this->render('BaBBackEndBundle:StyleBiere:show.html.twig', array(
            'styleBiere'        => $styleBiere,
            'refStr'            => $refString,
            'listBieres'        => $listBieres
        ));
    }

    public function listAction() {
        $em = $this->getDoctrine()->getManager();
        $styleBiereRepository = $em->getRepository('BaBCoreBundle:StyleBiere');
        $styleBieres = $styleBiereRepository->findAll();
        $countStyleBiere = array();
        $bieresRepository = $em->getRepository('BaBCoreBundle:Biere');
        foreach($styleBieres as $styleBiere) {
           $bieres = $bieresRepository->findBy(array(
               'style' => $styleBiere
           ));
            $styleBiere->setNombreBieres(count($bieres));
        }


        return $this->render('BaBBackEndBundle:StyleBiere:list.html.twig', array(
            'styleBiere'        => $styleBieres,
        ));
    }
}