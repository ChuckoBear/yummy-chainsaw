<?php
/**
 * Created by IntelliJ IDEA.
 * User: Gecko
 * Date: 06/08/2015
 * Time: 11:11
 */

namespace BaB\BackEndBundle\Controller;


use BaB\CoreBundle\Entity\ContactBrasserie;
use BaB\CoreBundle\Entity\MouvementStock;
use BaB\BackEndBundle\Form\ContactBrasserieType;
use BaB\BackEndBundle\Form\MouvementStockType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MouvementStockController extends Controller
{
    public function listAction(Request $requiest) {

        $em = $this->getDoctrine()->getManager();
        $mouvementStockRepository = $em->getRepository('BaBCoreBundle:MouvementStock');

        $listMouves = $mouvementStockRepository->findByEtatStockGroupedById();
        return $this->render('BaBBackEndBundle:MouvementStock:list.html.twig', array(
            'listMouves' => $listMouves
        ));
    }

    public function addAction(Request $request)
    {
        $mouvementStock = new MouvementStock();

        $form = $this->get('form.factory')->create(new MouvementStockType(), $mouvementStock);

        return $this->render('BaBBackEndBundle:MouvementStock:list.html.twig', array('form' => $form->createView()));
    }

    public function addMouvementAction(Request $request)
    {
        $contactBrasserie = new ContactBrasserie();

        $form = $this->get('form.factory')->create(new ContactBrasserieType(), $contactBrasserie);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contactBrasserie);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Brasserie bien enregistrée.');
            return $this->redirect($this->generateUrl('home'));
        }
        return null;
    }
}