<?php
/**
 * Created by IntelliJ IDEA.
 * User: chuck
 * Date: 01/12/15
 * Time: 11:04
 */

namespace BaB\BackEndBundle\Controller;



use BaB\BackEndBundle\Form\SouscriptionType;
use BaB\CoreBundle\Entity\Souscription;
use BaB\CoreBundle\Entity\SouscriptionDetail;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class SouscriptionController extends Controller
{
    public function addAction()
    {
        $souscription = new Souscription();
        $form = $this->get('form.factory')->create(new SouscriptionType(), $souscription);
        return $this->render('@BaBBackEnd/Souscription/form.html.twig',
            array(
                'form' => $form->createView()
            ));
    }

    /**
     * @param $id int
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function detailAction($id) {
        $em = $this->getDoctrine()->getEntityManager();
        $souscription = $em->getRepository('BaBCoreBundle:Souscription')->find($id);
        if (null != $souscription) {
            $souscription->setPointRelais(null);
            $form = $this->get('form.factory')->create(new SouscriptionType(), $souscription);
            return $this->render('@BaBBackEnd/Souscription/form.html.twig',
                array(
                    'form' => $form->createView(),
                    'id' => $id
                ));
        }
        return $this->redirectToRoute('souscription_list');
    }

    public function detailSubmitAction(Request $request, $id) {
        $em = $this->getDoctrine()->getEntityManager();
        $souscriptionRepository = $em->getRepository('BaBCoreBundle:Souscription');
        $souscription = $souscriptionRepository->find($id);

        if (null === $souscription) {
            throw new NotFoundHttpException("La souscription d'id" . $id . "n'existe pas.");
        }
        $souscription->setPointRelais(null);

        $form = $this->get('form.factory')->create(new SouscriptionType(), $souscription);

        if ($form->handleRequest($request)->isValid()) {
            $souscription->setPrixTotal($souscription->getModeSouscription()->getPrixDeVente() +
                $souscription->getModeSouscription()->getDuree() * $souscription->getModeLivraison()->getPrixDeVente());

            $em->persist($souscription);
            $this->retirerBoxDansCommande($souscription, $em);
            $this->ajoutBoxDansCommande($souscription, $em);

            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Modifications enregistrées.');
            return $this->redirectToRoute('souscription_list');
        }
        $request->getSession()->getFlashBag()->add('error', 'Echec des modifications');
        return $this->redirectToRoute('souscription_list');
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $souscriptionRepository = $em->getRepository('BaBCoreBundle:Souscription');
        $souscription = $souscriptionRepository->find($id);

        if (null === $souscription) {
            throw new NotFoundHttpException("La commande d'id" . $id . "n'existe pas.");
        }

        return $this->render('BaBBackEndBundle:Souscription:delete.html.twig', array(
            'souscription' => $souscription
        ));
    }

    public function deleteSubmitAction($id) {
        $em = $this->getDoctrine()->getManager();
        $souscription = $em->getRepository('BaBCoreBundle:Souscription')->find($id);
        if (null === $souscription) {
            throw new NotFoundHttpException("La commande d'id" . $id . "n'existe pas.");
        }
        $souscriptionDetails = $em->getRepository('BaBCoreBundle:SouscriptionDetail')->findBy(
            array('souscription' => $souscription)
        );
        foreach ($souscriptionDetails as $souscriptionDetail) {
            $em->remove($souscriptionDetail);
        }
        $em->remove($souscription);
        $em->flush();

        return $this->redirectToRoute('souscription_list');
    }

    public function addSouscriptionAction(Request $request) {
        $souscription = new Souscription();

        $form = $this->get('form.factory')->create(new SouscriptionType(), $souscription);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $souscription->setDebutSouscriptionDate(new \DateTime());
            $souscription->setPrixTotal($souscription->getModeSouscription()->getPrixDeVente() +
                $souscription->getModeSouscription()->getDuree() * $souscription->getModeLivraison()->getPrixDeVente());
            $em->persist($souscription);
            $this->ajoutBoxDansCommande($souscription, $em);

            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Commande bien enregistrée.');
            return $this->redirect($this->generateUrl('souscription_list'));
        }
        return null;
    }

    public function listAction() {
        $em = $this->getDoctrine()->getManager();
        $souscriptions = $em->getRepository('BaBCoreBundle:Souscription')->findBy(
            array(
            'etatCommande' => $em->getRepository('BaBCoreBundle:EtatCommande')->findAll()
            ), array('debutSouscriptionDate' => 'ASC')
        );

        return $this->render('BaBBackEndBundle:Souscription:list.html.twig', array(
            'souscriptions' => $souscriptions
        ));
    }

    public function ajoutBoxDansCommande(Souscription $souscription, EntityManager $em) {
        $boxs = $em->getRepository('BaBCoreBundle:Box')->findBy(array('gamme' => 1), null, $souscription
            ->getModeSouscription()->getDuree());
        foreach ($boxs as $box) {
            $souscriptionDetail = new SouscriptionDetail();
            $souscriptionDetail->setSouscription($souscription);
            $souscriptionDetail->setBox($box);
            $em->persist($souscriptionDetail);
        }
    }

    public function retirerBoxDansCommande(Souscription $souscription, EntityManager $em) {
        $commandeDetails =  $em->getRepository('BaBCoreBundle:SouscriptionDetail')->findBy(array(
            'souscription' => $souscription
        ));
        foreach ($commandeDetails as $box) {
            $em->remove($box);
        }
    }

    public function showAction($id) {
        $souscription = $this->getDoctrine()->getManager()->getRepository('BaBCoreBundle:Souscription')->find($id);
        if (null == $souscription) {
            throw $this->createNotFoundException("Commande non trouve");
        }
        $souscriptionDetails = $this->getDoctrine()->getManager()->getRepository('BaBCoreBundle:SouscriptionDetail')->findBySouscription($souscription);
        return $this->render("BaBBackEndBundle:Souscription:show.html.twig", array(
            "souscription" => $souscription,
            "souscriptionDetails" => $souscriptionDetails
        ));

    }
}

