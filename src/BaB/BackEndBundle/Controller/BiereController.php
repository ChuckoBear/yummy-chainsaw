<?php
/**
 * Created by IntelliJ IDEA.
 * User: Gecko
 * Date: 04/08/2015
 * Time: 16:49
 */

namespace BaB\BackEndBundle\Controller;

use BaB\CoreBundle\Entity\Biere;
use BaB\BackEndBundle\Form\BiereType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BiereController extends Controller
{

    public function addAction(Request $request)
    {
        $biere = new Biere();
        $form = $this->get('form.factory')->create(new BiereType(), $biere);
        return $this->render('BaBBackEndBundle:Biere:form.html.twig', array(
            'form' => $form->createView(),
            'mode' => 'Ajout'
        ));
    }

    public function addBeerAction(Request $request)
    {
        $biere = new Biere();

        $form = $this->get('form.factory')->create(new BiereType(), $biere);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($biere);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Biere bien enregistrée.');
            return $this->redirect($this->generateUrl('biere_list'));
        }
    }

    public function listAction() {
        $em = $this->getDoctrine()->getManager();
        $bieresRepository = $em->getRepository('BaBCoreBundle:Biere');
        $bieres = $bieresRepository->findAll();

        return $this->render('BaBBackEndBundle:Biere:list.html.twig', array(
            'bieres'=>$bieres));
    }

    public function viewAction($id) {
        $em = $this->getDoctrine()->getManager();
        $biereRepository = $em->getRepository('BaBCoreBundle:Biere');
        $biere = $biereRepository->find($id);
        if (null === $biere) {
            throw new NotFoundHttpException("La biere d'id" . $id . "n'existe pas.");
        }

        $mouvementRepository = $em->getRepository('BaBCoreBundle:MouvementStock');
        $mouvements = $mouvementRepository->findBy(array(
            'biere' => $biere
        ));
        $stock = 0;
        if(null !== $mouvements) {
            foreach($mouvements as $mouvement) {
                $stock += $mouvement->getQuantiteMouvementStock();
                $mouvement->setSoldeBieres($stock);
            }
        }

        return $this->render('@BaBBackEnd/Biere/show.html.twig', array(
            'biere' => $biere,
            'mouvements' => $mouvements,
            'etatStock' => $stock
            ));
    }

    public function updateAction($id) {
        $em = $this->getDoctrine()->getManager();
        $biereRepository = $em->getRepository('BaBCoreBundle:Biere');
        $biere = $biereRepository->find($id);

        if (null === $biere) {
            throw new NotFoundHttpException("La biere d'id" . $id . "n'existe pas.");
        }

        $form = $this->get('form.factory')->create(new BiereType(), $biere);
        return $this->render('BaBBackEndBundle:Biere:form.html.twig', array(
            'form'  => $form->createView(),
            'mode'  => 'Modification'
        ));
    }

    public function updateBeerAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $biereRepository = $em->getRepository('BaBBackEndBundle:Biere');
        $biere = $biereRepository->find($id);

        if (null === $biere) {
            throw new NotFoundHttpException("La biere d'id" . $id . "n'existe pas.");
        }

        $form = $this->get('form.factory')->create(new BiereType(), $biere);

        if ($form->handleRequest($request)->isValid()) {
            $em->persist($biere);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Modifications enregistrées.');
            return $this->redirect($this->generateUrl('biere_item', array(
                'id' => $id)
            ));
        }
        $request->getSession()->getFlashBag()->add('error', 'Echec des modifications');
        return $this->redirect($this->generateUrl('biere_list'));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $biereRepository = $em->getRepository('BaBCoreBundle:Biere');
        $biere = $biereRepository->find($id);

        if (null === $biere) {
            throw new NotFoundHttpException("La biere d'id" . $id . "n'existe pas.");
        }

        return $this->render('BaBBackEndBundle:Biere:delete.html.twig', array(
            'biere' => $biere
        ));

    }

    public function deleteBeerAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $biereRepository = $em->getRepository('BaBCoreBundle:Biere');
        $biere = $biereRepository->find($id);

        if (null === $biere) {
            throw new NotFoundHttpException("La biere d'id" . $id . "n'existe pas.");
        }

        $em->remove($biere);
        $em->flush();

        return $this->redirect($this->generateUrl('biere_list'));
    }
}
