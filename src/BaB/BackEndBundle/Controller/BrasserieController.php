<?php
/**
 * Created by IntelliJ IDEA.
 * User: Gecko
 * Date: 03/08/2015
 * Time: 15:23
 */

namespace BaB\BackEndBundle\Controller;

use BaB\CoreBundle\Entity\Brasserie;
use BaB\BackEndBundle\Form\BrasserieType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BrasserieController extends Controller
{
    /***
     * @param $id int L'identifiant de la brasserie
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $brasserieRepository = $em->getRepository('BaBCoreBundle:Brasserie');
        $brasserie = $brasserieRepository->find($id);
        if (null === $brasserie) {
            throw new NotFoundHttpException("La brasserie d'id" . $id . "n'existe pas.");
        }
        $listContacts = $em->getRepository('BaBCoreBundle:ContactBrasserie')
            ->findBy(array(
                'brasserie'     => $brasserie));
        $listBieres = $em->getRepository('BaBCoreBundle:Biere')
            ->findBy(array(
                'brasseur1'     => $brasserie
            ));
        return $this->render('BaBBackEndBundle:Brasserie:show.html.twig', array(
            'brasserie'         => $brasserie,
            'listContacts'      => $listContacts,
            'listBieres'        => $listBieres
        ));
    }

    public function addAction(Request $request)
    {
        $brasserie = new Brasserie();

        $form = $this->get('form.factory')->create(new BrasserieType(), $brasserie);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($brasserie);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Brasserie bien enregistrée.');
            return $this->redirect($this->generateUrl('brasserie_list'));
        }
        return $this->render('BaBBackEndBundle:Brasserie:form.html.twig', array(
            'form'              => $form->createView()
        ));
    }

    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $brasserieRepository = $em->getRepository('BaBCoreBundle:Brasserie');
        $brasseries = $brasserieRepository->findAll();

        $bieresRepository = $em->getRepository('BaBCoreBundle:Biere');
        $bierescount = array();
        foreach($brasseries as $brasserie) {
            $bieres = $bieresRepository
                ->findBy(array(
                    'brasseur1' => $brasserie->getId()
                ));
            $nombreBieres = count($bieres);
            $bierescount[$brasserie->getId()] = $nombreBieres;
        }



        return $this->render('BaBBackEndBundle:Brasserie:list.html.twig', array(
            'brasseries'        => $brasseries,
            'countBieres'       => $bierescount
        ));

    }


}