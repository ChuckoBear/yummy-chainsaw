<?php

namespace BaB\BackEndBundle\Controller;

use BaB\CoreBundle\Entity\Box;
use BaB\BackEndBundle\Form\BoxType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BoxController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('', array(
            'name'          => $name
        ));
    }

    public function addAction(Request $request) {
        $box = new Box();
        $form = $this->get('form.factory')->create(new BoxType(), $box);

        return $this->render('BaBBackEndBundle:Box:form.html.twig', array(
            'form'          => $form->createView()
        ));

    }

    public function listAction() {
        $em = $this->getDoctrine()->getManager();
        $boxRepository = $em->getRepository('BaBCoreBundle:Box');
        $boxs = $boxRepository->findAll();
        $count = 0;
        foreach ($boxs as $box) {

            $count = $count + $boxRepository->countBoxValidee($box);
            $box->setNombreBoxVendues($boxRepository->countBoxValidee($box));
        }

        return $this->render('BaBBackEndBundle:Box:list.html.twig', array(
            'boxs'          => $boxs
        ));
    }
}
